/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.

 * Copyright (C) 2007-2010  Intel Corporation. All rights reserved.
 * Copyright (C) 2012  Intel Corporation. All rights reserved.

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>
#include <net/if.h>

#include <dbus/dbus.h>
#include <glib.h>
#include <libvici.h>

/*
 * #error "Please define CONNMAN_API_SUBJECT_TO_CHANGE to acknowledge your
 * understanding that ConnMan hasn't reached a stable API."
 */
#define CONNMAN_API_SUBJECT_TO_CHANGE
#include <connman/plugin.h>
#include <connman/provider.h>
#include <connman/log.h>
#include <connman/task.h>
#include <connman/dbus.h>
#include <connman/inet.h>
#include <connman/agent.h>
#include <connman/setting.h>
#include <connman/vpn-dbus.h>

#include <sys/signalfd.h>
#include <vpn/vpn-provider.h>
#include <vpn/vpn-agent.h>

#include <vpn/plugins/vpn.h>
#include "libvici.h"
#include "ipsec_api.h"
#include "getaddr.h"
#include "vti_setup.h"
#include "pthread.h"
#include "fqdn_resolver.h"
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

static DBusConnection *connection;
static GMainLoop *main_loop = NULL;
static unsigned int __terminated = 0;
static GThread *listen_thread;
static pthread_mutex_t ipsecvici_conn_mutex;

#define CONN_COUNT 5
#define ERR_INVALID -1


enum command_format_options_t {
	COMMAND_FORMAT_NONE   = 0,
	COMMAND_FORMAT_RAW    = (1<<0),
	COMMAND_FORMAT_PRETTY = (1<<1),
	COMMAND_FORMAT_PEM    = (1<<2),
};

typedef enum command_format_options_t command_format_options_t;

#define IPSEC_INIT_STATE	     0
#define IPSEC_IKE_READY_STATE	   (1<<0)
#define IPSEC_CHILD_READY_STATE    (1<<1)
#define IPSEC_SUCCESS_STATE	   ((1<<0)|(1<<1))

typedef enum {
	IPSEC_E_CONN_DISCONNECTED = 0,
	IPSEC_E_CONN_DISCONNECTING,
	IPSEC_E_CONN_CONNECTED,
	IPSEC_E_CONN_CONNECTING,
	IPSEC_E_CONN_INVALID = -1,
} ipsecvici_conn_state_t;

typedef enum {
	IPSEC_E_ACTION_NO_CHANGE = 0,
	IPSEC_E_ACTION_CONNECT,
	IPSEC_E_ACTION_DISCONNECT,
	IPSEC_E_ACTION_INVALID = -1,
} ipsecvici_action_type_t;

struct ipsec_conn {
	char* vpn_name;		//e.g. NAD wifi ethernet
	char* conn_name;	//e.g. home_v4
	char* child_name;
	int ipsec_state;
	int conn_state;
	struct vpn_provider *provider;
	struct ipsecvici_private_data private_data;
} conns[CONN_COUNT];

typedef struct ipsec_conn ipsec_conn_t;

enum vpn_index_t {
	INDEX_NAD0	= 0,
	INDEX_NAD1	= 1,
	INDEX_NAD2	= 2,
	INDEX_ETH	= 3,
	INDEX_WIFI	= 4,
};

typedef enum vpn_index_t vpn_index_t;

/*
 * if contains	NAD0 return	0
 *		NAD1		1
 *		NAD2		2
 *		ethernet	3
 *		wifi		4
 */
static vpn_index_t get_vpn_name_type(const char* name)
{
	if (name == NULL) return -1;

	int len = strlen(name);
	int nad_l = strlen("NAD");
	int eth_l = strlen("ethernet");
	int wifi_l = strlen("wifi");
	int i;

	for (i = 0; i < len - nad_l; i++) {
		if (strncmp("NAD", name + i, nad_l) == 0) {
			if (*(name + i + nad_l) == (char)('0' + INDEX_NAD0))
				return INDEX_NAD0;
			if (*(name + i + nad_l) == (char)('0' + INDEX_NAD1))
				return INDEX_NAD1;
			if (*(name + i + nad_l) == (char)('0' + INDEX_NAD2))
				return INDEX_NAD2;
		}
	}

	for (i = 0; i <= len - eth_l; i++) {
		if (strncmp("ethernet", name + i, eth_l) == 0) return INDEX_ETH;
	}

	for (i = 0; i <= len - wifi_l; i++) {
		if (strncmp("wifi", name + i, wifi_l) == 0) return INDEX_WIFI;
	}

	return ERR_INVALID;
}

//Patch to parse local vip list
static int ipsecvici_local_ip_list_cb(void* data, vici_res_t* res, char* name, void* value, int len )
{
	if (data == NULL || name == NULL) return ERR_INVALID;

	int* index = (int*) data;
	DBG("local list is %s value is %s, index is %d\r\n", name, value, *index);

	if (*index > CONN_COUNT ||*index  < 0) {
		connman_error("index [%d] is wrong \r\n", *index);
		return ERR_INVALID;
	}
	if(0 == g_strcmp0("local-vips",name))
		conns[*index].private_data.local_vips = g_strndup((char *)value,len);
	DBG("value is %s\r\n", value);
	return 0;
}

//For IKE updown event
static int ipsecvici_ike_kv_cb(void* data, vici_res_t* res, char* name, void* value, int len )
{
	if (data == NULL || name == NULL) return ERR_INVALID;

	int* index = (int*) data;
	DBG("ike name is %s value is %.*s, index is %d\r\n",name, len, value, *index);

	if (*index > CONN_COUNT ||*index  < 0) {
		connman_error("index [%d] is wrong \r\n", *index);
		return ERR_INVALID;
	}

	if (0 == g_strcmp0("state", name)) {
		if (g_str_match_string("ESTABLISHED", (char *)value, 0)) {
			DBG("ike up %s success \r\n", (char* )value);
			conns[*index].ipsec_state |= IPSEC_IKE_READY_STATE;
		}
		else if (g_str_match_string("DELETING", (char *)value, 0)) {
			DBG("ike down %s success \r\n", (char* )value);
			conns[*index].ipsec_state &= ~IPSEC_IKE_READY_STATE;
		}
		else {
			DBG("msg value %s\r\n", (char* )value);
		}
		return 0;
	}
	if(0 == g_strcmp0("local-host",name))
	{
		conns[*index].private_data.local_host = g_strndup((char *)value,len);
	}
	else if(0 == g_strcmp0("remote-host",name))
	{
		conns[*index].private_data.remote_host = g_strndup((char *)value,len);
	}
	return 0;
}

//CHILD updown event
static int ipsecvici_child_kv_cb(void* data, vici_res_t* res, char* name, void* value, int len )
{
	if (data == NULL || name == NULL) return ERR_INVALID;

	int* index = (int*) data;
	DBG("child name is %s value is %s, index is %d\r\n", name, value, *index);

	if (*index > CONN_COUNT ||*index  < 0) {
		connman_error("index [%d] is wrong \r\n", *index);
		return ERR_INVALID;
	}

	if (0 == g_strcmp0("state", name)) {
		if (g_str_match_string("ESTABLISHED", (char *)value, 0)) {
			DBG("child up %s success \r\n", (char* )value);
			conns[*index].ipsec_state |= IPSEC_CHILD_READY_STATE;
		}
		else if (g_str_match_string("DELETING", (char *)value, 0)) {
			DBG("child down %s success \r\n", (char* )value);
			conns[*index].ipsec_state &= ~IPSEC_CHILD_READY_STATE;
		}
		else {
			DBG("msg value %s\r\n", (char* )value);
		}
		return 0;
	}

	if (0 == g_strcmp0("mark-in",name)) {
		DBG(" FOUND MARK INs\n");
		conns[*index].private_data.mark_in = g_strndup((char *)value,len);
	}

	return 0;
}

//CHILD SAS callback
static int ipsecvici_child_section_cb(void* data, vici_res_t* res, char* name)
{

	if (data == NULL || name == NULL) return ERR_INVALID;

	int* index = (int*) data;
	DBG("child-sas name is %s \r\n", name);

	if (*index > CONN_COUNT ||*index  < 0) {
		connman_error("index [%d] is wrong \r\n", *index);
		return ERR_INVALID;
	}

	if  (0 == g_strcmp0("child-sas",name)) {
		DBG("FOUND CHILD SAs\n");
		vici_parse_cb(res, ipsecvici_child_section_cb, NULL, NULL, index);
	}

	if  (0 == g_strcmp0(conns[*index].child_name, name)) {
		vici_parse_cb(res, NULL, ipsecvici_child_kv_cb, NULL, index);
	}

	return 0;
}

static int ipsecvici_conn_cb(void *user, vici_res_t *res, char *name)
{
	/*
	 * this connection is something like home_v4 ...
	 * by this name, we can find conns index.
	 */
	if (user == NULL || name == NULL) return ERR_INVALID;

	DBG("conn name is %s, event is %s\r\n", name ,user);
	int index = 0;
	for (index = 0; index < ARRAY_SIZE(conns); index++) {
		if (0 == g_strcmp0(conns[index].conn_name, name))
			break;
	}

	if (ARRAY_SIZE(conns) == index) {
		DBG("conn name not found\r\n");
		return ERR_INVALID;
	}

	if  (0 == g_strcmp0(user, "ike-updown"))
		vici_parse_cb(res, NULL, ipsecvici_ike_kv_cb, NULL, &index);
	if  (0 == g_strcmp0(user, "child-updown"))
		vici_parse_cb(res, ipsecvici_child_section_cb, ipsecvici_child_kv_cb, ipsecvici_local_ip_list_cb, &index);
	return 0;
}
static bool do_clean_jobs(int index)
{
	bool ret = FALSE;
	if (0 > index || index >= CONN_COUNT) {
		connman_error("invalid index = %d\n", index);
		return ERR_INVALID;
	}
	vti_cleanup(&conns[index].private_data);
	if(conns[index].private_data.table){
	g_free(conns[index].private_data.table);
	conns[index].private_data.table = NULL;
	}
	if(conns[index].private_data.local_host){
	g_free(conns[index].private_data.local_host);
	conns[index].private_data.local_host = NULL;
	}
	if(conns[index].private_data.remote_host){
	g_free(conns[index].private_data.remote_host);
	conns[index].private_data.remote_host = NULL;
	}
	if(conns[index].private_data.local_vips){
	g_free(conns[index].private_data.local_vips);
	conns[index].private_data.local_vips = NULL;
	}
	if(conns[index].private_data.mark_in){
	g_free(conns[index].private_data.mark_in);
	conns[index].private_data.mark_in = NULL;
	}
	if(conns[index].conn_name){
	g_free(conns[index].conn_name);
	conns[index].conn_name = NULL;
	}
	if(conns[index].conn_name){
	g_free(conns[index].child_name);
	conns[index].child_name = NULL;
	}
	fqdn_conn_delete(conns[index].provider);
	fqdn_remote_ip_discard(conns[index].provider);
	ret = TRUE;
	return ret;
}

static void get_ipsec_server_state(int* state)
{
	int index = 0;
	for (index = 0; index < CONN_COUNT; index++) {
		state[index] = conns[index].ipsec_state;
	}
	return ;
}

static void update_ipsec_action(int* action, const int* old_state, const int* new_state)
{
	int index = 0;
	for (index = 0; index < CONN_COUNT; index++) {
		if (old_state[index] == new_state[index]) {
			continue;
		}
		DBG( "state change: old_state:%d -> new_state: %d \n", old_state[index], new_state[index]);
		if (old_state[index] != IPSEC_SUCCESS_STATE && new_state[index] == IPSEC_SUCCESS_STATE) {
			DBG("do connect action");
			action[index] = IPSEC_E_ACTION_CONNECT;
		} else if ((old_state[index] & IPSEC_IKE_READY_STATE)
				&& ~(new_state[index] & IPSEC_IKE_READY_STATE) ) {
			DBG("do disconnect action");
			action[index] = IPSEC_E_ACTION_DISCONNECT;
		} else {
			DBG("no action");
		}
	}

	return ;
}

static void list_cb(void* user, char *name, vici_res_t *res)
{
	char buf[256];
	int old_server_state[CONN_COUNT] = {IPSEC_INIT_STATE};
	int new_server_state[CONN_COUNT] = {IPSEC_INIT_STATE};
	int action[CONN_COUNT] = {IPSEC_E_ACTION_NO_CHANGE};
	int err = 0, index = 0;
	bool ret;
	char *remote_ip = NULL;
	command_format_options_t *format = (command_format_options_t*)user;
	DBG("event name: %s happens\n", name);

	get_ipsec_server_state(old_server_state);

	vici_parse_cb(res, ipsecvici_conn_cb, NULL, NULL, name);

	get_ipsec_server_state(new_server_state);

	update_ipsec_action(action, old_server_state, new_server_state);
	/*
	 * if both ike and child are ready try vti here.
	 */

	for (index = 0; index < CONN_COUNT; index++) {
		DBG( "ipsecvici conns index:%d ipsec_state:%d conn_state:%d action:%d \n",
				index, conns[index].ipsec_state, conns[index].conn_state, action[index]);
		if (IPSEC_E_ACTION_CONNECT == action[index]) {
			DBG("start to do connection");

			pthread_mutex_lock(&ipsecvici_conn_mutex);

			/* connection jobs */
			if (conns[index].conn_state == IPSEC_E_CONN_CONNECTING) {
				/* vti setup */
				DBG( "do vti jobs\n");
				ret = vti_setup(&conns[index].private_data); //vti setup here
				if(ret==FALSE){
					do_clean_jobs(index);
					conns[index].ipsec_state = IPSEC_INIT_STATE;
					conns[index].conn_state = IPSEC_E_CONN_DISCONNECTED;
                                        vpn_provider_set_state(conns[index].provider,
                                                         VPN_PROVIDER_STATE_FAILURE);
                                        pthread_mutex_unlock(&ipsecvici_conn_mutex);
                                        goto out;

                                }
				/*
				 * The setting of provider ipaddr does not make much sense
				 * for ipsec tunnels because we do not have any tunnel device
				 * but this needs to be done because of the way provider works
				 * (so that ipconfig struct is created in provider).
				 */
				vpn_set_ifname(conns[index].provider,
						conns[index].private_data.vpn_name);
				remote_ip = fqdn_get_remote_ip(conns[index].provider);
				if (remote_ip == NULL) {
					do_clean_jobs(index);
                                        conns[index].ipsec_state = IPSEC_INIT_STATE;
				        conns[index].conn_state = IPSEC_E_CONN_DISCONNECTED;
					vpn_provider_set_state(conns[index].provider,
                                                               VPN_PROVIDER_STATE_FAILURE);
					pthread_mutex_unlock(&ipsecvici_conn_mutex);
					goto out;
                                }
				DBG("Server addr %s\n",remote_ip);
				int ai_family = get_af_family();
				DBG("Address family %d\n",ai_family);
				switch (ai_family) {
					case AF_INET:
						connman_ipaddress_set_ipv4(ipaddr_connman, conns[index].private_data.local_vips, NULL, remote_ip);
						break;
					case AF_INET6:
						connman_ipaddress_set_ipv6(ipaddr_connman, remote_ip, 128, remote_ip);
						break;
					default:
						DBG("not supported network family %d",
								ai_family);
						do_clean_jobs(index);
						conns[index].ipsec_state = IPSEC_INIT_STATE;
						conns[index].conn_state = IPSEC_E_CONN_DISCONNECTED;
						vpn_provider_set_state(conns[index].provider,
								VPN_PROVIDER_STATE_FAILURE);
						pthread_mutex_unlock(&ipsecvici_conn_mutex);
						goto out;
				}

				vpn_provider_set_ipaddress(conns[index].provider, ipaddr_connman);

				/*
				 * Set the provider state properly, otherwise service will disconnect
				 * it after 2 minutes (CONNECT_TIMEOUT in service.c)
				 */
				conns[index].conn_state = IPSEC_E_CONN_CONNECTED;
				vpn_update_after_connected(conns[index].provider);
				fqdn_conn_add(conns[index].provider);
			}
			pthread_mutex_unlock(&ipsecvici_conn_mutex);
		} /* IPSEC_E_ACTION_CONNECT == action[index] */
		else if (IPSEC_E_ACTION_DISCONNECT == action[index]) {
			/* disconnection jobs */
			DBG("start to do disconnection \n");
			pthread_mutex_lock(&ipsecvici_conn_mutex);
			if (conns[index].conn_state != IPSEC_E_CONN_DISCONNECTED
				&& conns[index].conn_state != IPSEC_E_CONN_DISCONNECTING) {
				DBG( "do disconnection \n");
				if (NULL == conns[index].provider) {
					pthread_mutex_unlock(&ipsecvici_conn_mutex);
					goto out;
				}
				conns[index].conn_state = IPSEC_E_CONN_DISCONNECTING;
				const char* prov_name = vpn_provider_get_string(conns[index].provider, "Name");

				DBG(" prov name is %s, index is %d, conn_name is %s\r\n",
								prov_name ? prov_name : "(nil)", index, conns[index].conn_name);

				do_clean_jobs(index);

				conns[index].ipsec_state = IPSEC_INIT_STATE;
				conns[index].conn_state = IPSEC_E_CONN_DISCONNECTED;
				vpn_provider_set_state(conns[index].provider, VPN_PROVIDER_STATE_DISCONNECT);
				vpn_provider_set_state(conns[index].provider, VPN_PROVIDER_STATE_IDLE);

				vpn_provider_set_string(conns[index].provider, "ipsecvici.Password", NULL);

				vpn_state_update_after_disconnect(conns[index].provider);
			}
			pthread_mutex_unlock(&ipsecvici_conn_mutex);
		} /* IPSEC_E_ACTION_DISCONNECT == action[index] */
		else {
			// do nothing
		}
	}
out:
	return;
}

/*
 *  A thread to listen notification coming from strongswan
 */
static void* listen_event()
{
	int ret = 0;
	command_format_options_t format = COMMAND_FORMAT_NONE;
	vici_conn_t *conn;
	guint signal;
	int i,retries = 30;

	DBG("ipsecvici listening daemon enter.ver3\n");

	format |= COMMAND_FORMAT_PRETTY;
	format |= COMMAND_FORMAT_RAW;

	vici_init();

	/*
	 *  charon default socket may not ready by now
	 */
	for (i = 0; i < retries; i++) {
		DBG("ipsecvici before connect .\r\n");
		conn = vici_connect(NULL);
		DBG("ipsecvici after connect .\r\n");
		if (conn)
			break;
		else
			usleep(300*1000);
	}

	if (i == retries) {
		ret = errno;
		DBG("ipsecvici connecting to default URI failed %s",strerror(errno));
		goto out;
	}

	main_loop = g_main_loop_new(NULL, FALSE);
	if (vici_register(conn, "ike-updown", list_cb, &format) != 0) {
		fprintf(stderr, "ipsecvici registering for IKE_SAs failed: %s\n",
				strerror(errno));
		return NULL;
	}
	if (vici_register(conn, "child-updown", list_cb, &format) != 0) {
		fprintf(stderr, "ipsecvici registering for CHILD_SAs failed: %s\n",
				strerror(errno));
		return NULL;
	}

	DBG("ipsecvici enter main loop run.\r\n");
	g_main_loop_run(main_loop);

	vici_disconnect(conn);
out:
	DBG("ipsecvici listening daemon exit.\r\n");
	return NULL;
}

static int ipsecvici_notify(DBusMessage *msg, struct vpn_provider *provider)
{
	/*
	 *  legacy API, just to compatible with vpn structure
	 */
	DBG("warning: ipsecvici_notify is called \r\n");
	return VPN_STATE_CONNECT;
}

void sys_dreg()
{
	char *cmd;
	cmd = g_strdup("dreg");
	int result = system(cmd);
	g_free(cmd);
	if (result == -1) {
		DBG("DREG failed\n");
	}
	else if (WEXITSTATUS(result) == 127)
		DBG("That shell command is not found\n");
	else
		DBG("DREG successful\n");
	return;
}


bool secret_load()
{
	char *cmd;
	cmd = g_strdup("swanctl -s");
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else {
		DBG("swanctl -s loaded\n");
		return TRUE;
	}
}

static int ipsecvici_connect(struct vpn_provider *provider,
			struct connman_task *task, const char *if_name,
			vpn_provider_connect_cb_t cb, const char *dbus_sender,
			void *user_data)
{

	/*
	 * get vpn name and conf file
	 * config file should be located in /etc/swanctl/swanctl.conf+conf_name
	 */
	char* conf_name = NULL;
	char* vpn_name = NULL;
	bool ret1;
	char* tmp_conn_name = NULL;
	char* tmp_mark_in = NULL;
	char* tmp_child_name = NULL;
	int state = IPSEC_INIT_STATE;
	int res;
	char *remote_ip = NULL;
	bool ret_cleanup;
	const char* prov_name = vpn_provider_get_string(provider, "Name");
	int ret = -1;
	vici_conn_t* conn;
	int idx = get_vpn_name_type(prov_name);
	if (idx == ERR_INVALID) {
		DBG("ipsecvici wrong vpn name: %s\n", prov_name ? prov_name : "(nil)");
		ret = -1;
		goto rcleannone;
	}
	vpn_name = conns[idx].vpn_name;
	conf_name = vpn_name;
	DBG("ipsecvici get vpn name %s\n", vpn_name);

	conns[idx].provider = provider;
	// get current IP addr
	char* ipaddr = find_ipaddress_by_service_keyname(vpn_name,
					 conns[idx].provider);

	if (ipaddr == NULL) {
		fprintf(stderr, "ipsecvici can't get ip address\n");
		connman_error("ipsecvici can't get ip address\n");
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
		goto rcleannone;
	}
	DBG("ipsecvici get ip addr %s\n", ipaddr);

	bool ret_fqdn = fqdn_resolver_start(conns[idx].provider);
	if(ret_fqdn == TRUE){
		remote_ip = fqdn_get_remote_ip(conns[idx].provider);
                if (remote_ip == NULL) {
                connman_error("ipsecvici can't get remote ip address\n");
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		ret = -1;
		goto rcleannone;
                }
		DBG("Remote ip = %s",remote_ip);
	}
	else{
                ret = -1;
                goto rcleannone;
	}

	/*
	 * create a conn object for further vici operations
	 * for win32, vici_connect default link to URI "tcp://127.0.0.1:6543"
	 * otherwise, link to URI "unix:///tmp/strongswan-vici-request-test"
	 */
	conn = vici_connect(NULL);
	if (conn == NULL) {
		fprintf(stderr, "ipsecvici vici_connect failed\n");
		connman_error("ipsecvici vici_connect failed\n");
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
		goto rcleanip;
	}

	/*
	 * load ipsec configuration
	 * input is ipaddr and conf_name
	 * besides conn name(e.g. home_v4) will be assigned into conns[idx]
	 * during parsing
	 * same to swanctl --load-conns + swanctl --load-creds
	 * get mark in from event message not from config file
	 * still keep the mark_in-config-file-reading function but no more buffer copy
	 */
	sys_dreg();
	ret = load_conns(conn, conf_name, ipaddr,remote_ip, &tmp_conn_name, &tmp_child_name,
			&tmp_mark_in);
	if (!!ret) {
		connman_error("ipsecvici load_conns failed : ret=%d\n", ret);
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
		goto rcleanip;
	}

	ret1 = secret_load();
	if (ret1 == TRUE) {
		DBG("Secret load successful");
	}
	else {
		DBG("Secret load failed");
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
		goto rcleanip;
	}
	conns[idx].provider = provider;
	if (tmp_conn_name && tmp_child_name) {
		DBG("Conn obtained\n");
		conns[idx].conn_name = g_strndup(tmp_conn_name, strlen(tmp_conn_name));
		conns[idx].child_name= g_strndup(tmp_child_name, strlen(tmp_child_name));
	}
	else {
		DBG("Conn load error\n");
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
		goto rcleanip;

	}
	DBG("ipsecvici get conn_name %s child_name %s mark_in %s idx %d\n",
			conns[idx].conn_name, conns[idx].child_name,
			conns[idx].private_data.mark_in ? conns[idx].private_data.mark_in : "(nil)", idx);

	// to restore old status when initiate failed.
	pthread_mutex_lock(&ipsecvici_conn_mutex);
	state = conns[idx].conn_state;
	conns[idx].conn_state = IPSEC_E_CONN_CONNECTING;
	pthread_mutex_unlock(&ipsecvici_conn_mutex);
	/*
	 * start connecting by initiate the corresponding conn name
	 * same to swanctl --initiate --child conn_name
	 */
	DBG("call initiate\n");
	ret = initiate(conn, conns[idx].child_name);
	if (!!ret) {
		connman_error("ipsecvici initiate failed : old_state=%d, ret=%d\n",
				state, ret);
		res = terminate(conn, conns[idx].conn_name);
		do_clean_jobs(idx);
		connman_error("ipsecvici terminate res = %d\n", res);
		vpn_provider_set_state(conns[idx].provider,VPN_PROVIDER_STATE_FAILURE);
		pthread_mutex_lock(&ipsecvici_conn_mutex);
		conns[idx].ipsec_state = IPSEC_INIT_STATE;
		conns[idx].conn_state = IPSEC_E_CONN_DISCONNECTED;
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		ret = -1;
	}
	vici_disconnect(conn);
rcleanip:
	free(ipaddr);
rcleannone:

	return ret;
}

static int ipsecvici_error_code(struct vpn_provider *provider, int exit_code)
{
	switch (exit_code) {
	case 1:
		return CONNMAN_PROVIDER_ERROR_CONNECT_FAILED;
	default:
		return CONNMAN_PROVIDER_ERROR_UNKNOWN;
	}
}

static void ipsecvici_disconnect(struct vpn_provider *provider)
{
	DBG("ipsecvici disconnect!\n");

	// find the corresponding conn
	vici_conn_t* conn;
	int res;
	bool ret;
	const char* prov_name = vpn_provider_get_string(provider, "Name");

	int idx = get_vpn_name_type(prov_name);
	if (idx == -1) {
		connman_error("wrong vpn name: %s\n", prov_name ? prov_name : "(nil)");
		fprintf(stderr, "wrong vpn name: %s\n", prov_name);
		return;
	}
	DBG(" prov name is %s, idx is %d, conn_name is %s\r\n",
					prov_name, idx, conns[idx].conn_name);

	pthread_mutex_lock(&ipsecvici_conn_mutex);
	if (IPSEC_E_CONN_DISCONNECTING == conns[idx].conn_state
			|| IPSEC_E_CONN_DISCONNECTED == conns[idx].conn_state) {
		connman_error("is under disconnecting, state = %d\n",  conns[idx].conn_state);
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		return;
	}
	conns[idx].conn_state = IPSEC_E_CONN_DISCONNECTING;
	pthread_mutex_unlock(&ipsecvici_conn_mutex);
	// create a conn object for further vici operations
	conn = vici_connect(NULL);
	if (conn == NULL) {
		connman_error("ipsecvici vici_connect failed\n");
		fprintf(stderr, "ipsecvici vici_connect failed\n");
		do_clean_jobs(idx);
		goto reset_state;
	}


	DBG("Before VTI cleanup\n");
        vti_cleanup(&conns[idx].private_data);
        DBG("After VTI cleanup\n");

	// terminate connection based on its conn name
	// same to swanctl --terminate --ike conn_name
	res = terminate(conn, conns[idx].conn_name);
	DBG("disconnect terminate prov name is %s, idx is %d, conn_name is %s\r\n",
					prov_name, idx, conns[idx].conn_name);
	if (res != 0) {
		fprintf(stderr, "ipsecvici terminate failed\n");
		connman_error("%s: ERR : terminate failed %s : but restore status anyway",
				__func__, strerror(res));
		/* not return, keep clean status */
	}

	if(conns[idx].private_data.table){
        g_free(conns[idx].private_data.table);
        conns[idx].private_data.table = NULL;
        }
        if(conns[idx].private_data.local_host){
        g_free(conns[idx].private_data.local_host);
        conns[idx].private_data.local_host = NULL;
        }
        if(conns[idx].private_data.remote_host){
        g_free(conns[idx].private_data.remote_host);
        conns[idx].private_data.remote_host = NULL;
        }
        if(conns[idx].private_data.local_vips){
        g_free(conns[idx].private_data.local_vips);
        conns[idx].private_data.local_vips = NULL;
        }
        if(conns[idx].private_data.mark_in){
        g_free(conns[idx].private_data.mark_in);
        conns[idx].private_data.mark_in = NULL;
        }
        if(conns[idx].conn_name){
        g_free(conns[idx].conn_name);
        conns[idx].conn_name = NULL;
        }
        if(conns[idx].conn_name){
        g_free(conns[idx].child_name);
        conns[idx].child_name = NULL;
        }


	vici_disconnect(conn);
reset_state:
	pthread_mutex_lock(&ipsecvici_conn_mutex);
	conns[idx].ipsec_state = IPSEC_INIT_STATE;
	conns[idx].conn_state = IPSEC_E_CONN_DISCONNECTED;
	pthread_mutex_unlock(&ipsecvici_conn_mutex);
	vpn_provider_set_state(conns[idx].provider, VPN_PROVIDER_STATE_DISCONNECT);
	fqdn_conn_delete(conns[idx].provider);
 	fqdn_remote_ip_discard(conns[idx].provider);
	vpn_provider_set_string(provider, "ipsecvici.Password", NULL);

    return;
}

static int ipsecvici_save(struct vpn_provider *provider, GKeyFile *keyfile)
{
	return 0;
}

static int ipsecvici_is_conn_ready(struct vpn_provider *provider)
{
	const char* prov_name = vpn_provider_get_string(provider, "Name");

	int idx = get_vpn_name_type(prov_name);
	if (idx == -1) {
		DBG("wrong vpn name: %s\n", prov_name ? prov_name : "(nil)");
		fprintf(stderr, "wrong vpn name: %s\n", prov_name);
		return -1;
	}

	pthread_mutex_lock(&ipsecvici_conn_mutex);
	if (IPSEC_E_CONN_CONNECTED == conns[idx].conn_state) {
		pthread_mutex_unlock(&ipsecvici_conn_mutex);
		return 0;
	}
	pthread_mutex_unlock(&ipsecvici_conn_mutex);
	return 1;
}

static struct vpn_driver vpn_driver = {
	.flags		= VPN_FLAG_NO_TUN,
	.notify		= ipsecvici_notify,
	.connect	= ipsecvici_connect,
	.error_code	= ipsecvici_error_code,
	.save		= ipsecvici_save,
	.disconnect	= ipsecvici_disconnect,
	.is_conn_ready= ipsecvici_is_conn_ready,
};

static int ipsecvici_init(void)
{
	int i;
	DBG("ipsecvici_init!\n");
	connection = connman_dbus_get_connection();

	conns[0].vpn_name = "NAD0";
	conns[1].vpn_name = "NAD1";
	conns[2].vpn_name = "NAD2";
	conns[3].vpn_name = "ethernet";
	conns[4].vpn_name = "wifi";
	conns[0].private_data.vpn_name = "vpn_NAD0";
	conns[1].private_data.vpn_name = "vpn_NAD1";
	conns[2].private_data.vpn_name = "vpn_NAD2";
	conns[3].private_data.vpn_name = "vpn_ethernet";
	conns[4].private_data.vpn_name = "vpn_wifi";
	for (i = 0; i < CONN_COUNT; i++) {
		conns[i].conn_name = NULL;
		conns[i].child_name = NULL;
	}
	pthread_mutex_init(&ipsecvici_conn_mutex, NULL);
	conn_tracker = g_hash_table_new(g_str_hash,g_str_equal);
	if(conn_tracker)
		DBG("Hash table created\n");
	else
		DBG("Hash table NULL\n");
	vici_init();
	/*
	 * create a thread for notification
	 */
	listen_thread = g_thread_new("listening_thread", listen_event, NULL);
	if (listen_thread == NULL) {
		DBG("ipsec failed to create listen thread\r\n");
	}
	DBG("ipsecvici_init finish!\n");

	return vpn_register("ipsecvici", &vpn_driver, "/usr/sbin/ipsec");
}

static void ipsecvici_exit(void)
{
	pthread_mutex_destroy(&ipsecvici_conn_mutex);
	vpn_unregister("ipsecvici");
	if (listen_thread)
		g_thread_exit(listen_thread);
	vici_deinit();
	dbus_connection_unref(connection);
}

CONNMAN_PLUGIN_DEFINE(ipsecvici, "ipsecvici plugin", VERSION,
	 CONNMAN_PLUGIN_PRIORITY_DEFAULT, ipsecvici_init, ipsecvici_exit)
