/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.

 * * Neither the name of The Linux Foundation nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.

 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "vti_setup.h"

bool tunnel_add(char *str1,char *str2,char* str3,char *str4){
	char *cmd;
	int mark;
	DBG("Enter tunnel add mark-in %s\n", str4);
	if (!str4) {
		connman_error("error mark-in is null return");
		return FALSE;
	}
	sscanf(str4,"%x",&mark);
	sprintf(str4, "%d", mark);
	cmd = g_strdup_printf("ip tunnel add %s local %s remote %s mode vti key %s ", str1, str2, str3, str4);
	DBG("LOCAL:%s,REMOTE:%s TUNNEL ADDED iface:%s",str2,str3,str1);
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else {
		DBG("ip tunnel add %s local %s remote %s mode vti key %s ",str1,str2,str3,str4);
		return TRUE;
	}
}



bool ip_link(char *str1){
	char *cmd;
	cmd = g_strdup_printf("ip link set %s up", str1);
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else{
		DBG("iface %s LINK SETUP",str1);
		return TRUE;
	}
}

bool addr_add(char *str1,char *str2){
	char *cmd;
	cmd = g_strdup_printf("ip addr add %s dev %s", str1,str2);
	int result = system(cmd);
	g_free(cmd);
	DBG("Result: %d\n",result);
	if (result != 0)
		return FALSE;
	else{
		DBG("ADDR:%s,IFACE:%s TUNNEL ADDR ADDED",str1,str2);
		return TRUE;
	}
}

bool rule_add(char *str1,char *str2){
	char *cmd;
	cmd = g_strdup_printf("ip rule add from %s table %s", str1,str2);
	int result = system(cmd);
	g_free(cmd);
	DBG("Result: %d\n",result);
	if (result != 0)
		return FALSE;
	else{
		DBG("ip:%s,table:%s RULE ADDED",str1,str2);
		return TRUE;
	}
}

bool route_add(char *str1,char *str2){
	char *cmd;
	cmd = g_strdup_printf("ip route add default dev %s table %s", str1,str2);
	int result = system(cmd);
	g_free(cmd);
	DBG("Result: %d\n",result);
	if (result != 0)
		return FALSE;
	else{
		DBG("iface:%s,table:%s ROUTE ADDED",str1,str2);
		return TRUE;
	}
}



bool command_sysclt(char *str1){
	char *cmd;
	cmd = g_strdup_printf("sysctl -w net.ipv4.conf.%s.disable_policy=1", str1);
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else{
		DBG("iface:%s SYSCTL",str1);
		return TRUE;
	}
}


char *check_table(){
	char cmd[] ="ip rule list | awk \'/lookup/ {print $NF}\'";
	FILE *fp;
	char buf[10000];
	int flag=0;
	DBG("Enter check table\n");
	char *table_char_num = (char *)malloc(10*sizeof(char));
	GHashTable *ip_table;
	ip_table = g_hash_table_new(g_str_hash,g_str_equal);
	fp = popen(cmd,"r");
	if (NULL == fp) {
		perror("fp");
		exit(1);
	}
	long ret;
	while (fgets(buf, sizeof(buf)-1, fp) != NULL) {
		DBG("Existing tables: %s\n",buf);
		g_hash_table_insert(ip_table,buf,"1");
	}
	int num;
	while(!flag){
		memset(table_char_num,0,sizeof(table_char_num));
		num = g_random_int_range(1000,10000);
		snprintf(table_char_num,10,"%d",num);
		DBG("Table number %d\n",num);
		DBG("Table char number %s\n",table_char_num);
		if(!g_hash_table_contains(ip_table,table_char_num))
			flag=1;
	}
	pclose(fp);
	DBG("TABLE NUM:%s OBTAINED",table_char_num);
	return table_char_num;
}

bool tunnel_del(char *str1){
	char *cmd;
	cmd = g_strdup_printf("ip tunnel del %s", str1);
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else{
		DBG("iface:%s TUNNEL DELETED",str1);
		return TRUE;
	}
}

bool del_table(char *str1){
	char *cmd;
	cmd = g_strdup_printf("ip rule del table %s", str1);
	int result = system(cmd);
	g_free(cmd);
	if (result != 0)
		return FALSE;
	else{
		DBG("TABLE:%s DELETED",str1);
		return TRUE;
	}
}

void vti_cleanup(struct ipsecvici_private_data *priv){
	bool ret;
	ret=tunnel_del(priv->vpn_name);
	if(ret == FALSE) {
		DBG("Tunnel delete failed");
	}

	if (priv->table) {
	ret=del_table(priv->table);
	if(ret == FALSE) {
		DBG("Table delete failed");
	}
	}
	return;
}

bool vti_setup(struct ipsecvici_private_data *priv){
	bool ret;
	if (NULL == priv) {
		goto out;
	}
	priv->table = check_table();
	if (NULL == priv->table) {
		goto out;
	}
	DBG("Before tunnel add\n");
	ret=tunnel_add(priv->vpn_name,priv->local_host,priv->remote_host,priv->mark_in);
	if(ret == FALSE)
	{
		DBG("Tunnel add failed\n");
		goto out;
	}
	ret=ip_link(priv->vpn_name);
	if(ret == FALSE)
	{
		DBG("Link set up failed\n");
		goto out;
	}

	ret=addr_add(priv->local_vips,priv->vpn_name);
	if(ret == FALSE)
	{
		DBG("Address add failed\n");
		goto out;
	}
	ret=command_sysclt(priv->vpn_name);
	if(ret == FALSE)
	{
		DBG("Sysctl failed\n");
		goto out;
	}
	ret=rule_add(priv->local_vips,priv->table);
	if(ret == FALSE)
	{
		DBG("Rule add failed\n");
		goto out;
	}
	ret=route_add(priv->vpn_name,priv->table);
	if(ret == FALSE)
	{
		DBG("Route add failed\n");
		goto out;
	}
out:
	return ret;
}
