/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.

 * * Neither the name of The Linux Foundation nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.

 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "fqdn_resolver.h"

struct addrinfo tips, *pres;
int ai_family;

GHashTableIter iter;
gpointer key, value;

static void fqdn_display_states(tracker_value *fqdn);

void resolver_init(tracker_value **fqdn_value) {
   memset((*fqdn_value)->remote_ip, 0, sizeof((*fqdn_value)->remote_ip));
   (*fqdn_value)->NAD0 = FALSE;
   (*fqdn_value)->NAD1 = FALSE;
   (*fqdn_value)->NAD2 = FALSE;
   (*fqdn_value)->ethernet = FALSE;
   (*fqdn_value)->wifi = FALSE;
}


bool fqdn_resolver_start(struct vpn_provider *provider) {
   struct sockaddr_in *addr;
   const char *host_fqdn = NULL;
   bool ret;
   bool res;
   int err = 0;
   const char *error;
   host_fqdn = g_strdup(vpn_provider_get_string(provider, "Host"));

   DBG("Host obtained = %s\n", host_fqdn);
   int size2 = g_hash_table_size(conn_tracker);
   DBG("Table size before lookup %d\n", size2);

   DBG("Printing hash table before resolution\n");
   g_hash_table_iter_init(&iter, conn_tracker);
   while (g_hash_table_iter_next(&iter, &key, &value)) {
      DBG("Key: %s\n", key);
      DBG("Value: %p\n", value);
   }

   if (g_hash_table_contains(conn_tracker, host_fqdn)) {
      DBG("Contains FQDN\n");
   }
   tracker_value *temp = g_hash_table_lookup(conn_tracker, host_fqdn);
   DBG("Lookup fqdn_resolver\n");
   if (!temp) {
      temp  = (tracker_value *)g_malloc(sizeof(tracker_value));
      resolver_init(&temp);
      DBG("Temp resolved\n");
   }
   DBG("Host lookup complete\n");
   fqdn_display_states(temp);
   if (g_strcmp0(temp->remote_ip,"")==0) {
      DBG("Remote ip is NULL\n");
      memset(&tips, 0, sizeof(tips));
      tips.ai_family = AF_INET;
      tips.ai_flags = 0;
      err = getaddrinfo(host_fqdn, NULL, &tips, &pres);
      error = gai_strerror(err);
      if (err) {
         DBG("Error getaddrinfo : %s", error);
         connman_error("DNS resolver can't get ip address\n");
         vpn_provider_set_state(provider, VPN_PROVIDER_STATE_FAILURE);
         res = FALSE;
         goto out;
      }
      DBG("Pres family %d\n", pres->ai_family);
      DBG("Getaddrinfo() completed\n");
      ai_family = pres->ai_family;
      ipaddr_connman = connman_ipaddress_alloc(pres->ai_family);
      addr = (struct sockaddr_in *)pres->ai_addr;
      g_strlcpy(temp->remote_ip, inet_ntoa((struct in_addr)addr->sin_addr), sizeof(temp->remote_ip));
      g_hash_table_insert(conn_tracker, (gpointer)host_fqdn, temp);
      DBG("FQDN inserted = %s\n", host_fqdn);
      DBG("remote ip obtained from FQDN resolution = %s\n", temp->remote_ip);
      freeaddrinfo(pres);
   }


   DBG("Printing hash table after resolution\n");
   g_hash_table_iter_init(&iter, conn_tracker);
   while (g_hash_table_iter_next(&iter, &key, &value)) {
      DBG("Key: %s\n", key);
      DBG("Value: %p\n", value);
   }
   res = TRUE;
   DBG("FQDN resolver printing struct");
   int size = g_hash_table_size(conn_tracker);
   DBG("Table size after resolution= %d", size);
   fqdn_display_states(temp);

out:
   return res;
}

bool fqdn_conn_add(struct vpn_provider *provider) {
   bool res;
   const char *host_fqdn = NULL;
   host_fqdn = vpn_provider_get_string(provider, "Host");
   DBG("Lookup update_conn_add %s\n", host_fqdn);
   tracker_value *temp = g_hash_table_lookup(conn_tracker, host_fqdn);
   const char *NAD_name = NULL;
   NAD_name = vpn_provider_get_string(provider, "Name");
   DBG("NAD name:%s", NAD_name);


   if (g_strrstr(NAD_name, "vpn_NAD0")) {
      if (temp->NAD0 == FALSE) {
         temp->NAD0 = TRUE;
      } else {
         DBG("Connection %s is already connected", NAD_name);
         res = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_NAD1")) {
      if (temp->NAD1 == FALSE) {
         temp->NAD1 = TRUE;
      } else {
         DBG("Connection %s is already connected", NAD_name);
         res = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_NAD2")) {
      if (temp->NAD2 == FALSE) {
         temp->NAD2 = TRUE;
      } else {
         DBG("Connection %s is already connected", NAD_name);
         res = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_ethernet")) {
      if (temp->ethernet == FALSE) {
         temp->ethernet = TRUE;
      } else {
         DBG("Connection %s is already connected", NAD_name);
         res = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_wifi")) {
      if (temp->wifi == FALSE) {
         temp->wifi = TRUE;
      } else {
         DBG("Connection %s is already connected", NAD_name);
         res = FALSE;
         goto out;
      }
   } else {
      DBG("Wrong connection name\n");
      res = FALSE;
      goto out;
   }



   res = TRUE;
   DBG("Update connection add\n");
   fqdn_display_states(temp);

out:
   return res;

}




int get_af_family() {
   return ai_family;
}

char* fqdn_get_remote_ip(struct vpn_provider *provider) {
   const char *host_fqdn = NULL;
   host_fqdn = vpn_provider_get_string(provider, "Host");
   DBG("Lookup get_remoteip %s\n", host_fqdn);
   int size2 = g_hash_table_size(conn_tracker);
   DBG("Table size before lookup %d\n", size2);

   DBG("Printing hash table get remote ip\n");
   g_hash_table_iter_init(&iter, conn_tracker);
   while (g_hash_table_iter_next(&iter, &key, &value)) {
      DBG("Key: %s\n", key);
      DBG("Value: %p\n", value);
   }

   tracker_value *return_val = g_hash_table_lookup(conn_tracker, host_fqdn);
   DBG("return_val %p\n", return_val);

   if (return_val) {
      DBG("remote ip from fqdn_resolver %s\n", return_val->remote_ip);
      return return_val->remote_ip;
   } else return NULL;
}


bool fqdn_conn_delete(struct vpn_provider *provider) {
   const char *host_fqdn = NULL;
   bool ret;
   host_fqdn = vpn_provider_get_string(provider, "Host");
   ret = g_hash_table_contains(conn_tracker, host_fqdn);
   if (ret == FALSE) {
      goto out;
   }
   DBG("Lookup conn_tracker\n");
   tracker_value *fqdn_value = g_hash_table_lookup(conn_tracker, host_fqdn);
   if (fqdn_value->remote_ip == NULL) {
      DBG("IP is already NULL\n");
      ret = FALSE;
      goto out;
   }
   DBG("Print states at update\n");
   fqdn_display_states(fqdn_value);
   const char *NAD_name = NULL;
   NAD_name = vpn_provider_get_string(provider, "Name");
   if (g_strrstr(NAD_name, "vpn_NAD0")) {
      if (fqdn_value->NAD0 == TRUE) {
         fqdn_value->NAD0 = FALSE;
      } else {
         DBG("Connection %s is already disconnected", NAD_name);
         ret = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_NAD1")) {
      if (fqdn_value->NAD1 == TRUE) {
         fqdn_value->NAD1 = FALSE;
      } else {
         DBG("Connection %s is already disconnected", NAD_name);
         ret = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_NAD2")) {
      if (fqdn_value->NAD2 == TRUE) {
         fqdn_value->NAD2 = FALSE;
      } else {
         DBG("Connection %s is already disconnected", NAD_name);
         ret = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_ethernet")) {
      if (fqdn_value->ethernet == TRUE) {
         fqdn_value->ethernet = FALSE;
      } else {
         DBG("Connection %s is already disconnected", NAD_name);
         ret = FALSE;
         goto out;
      }
   } else if (g_strrstr(NAD_name, "vpn_wifi")) {
      if (fqdn_value->wifi == TRUE) {
         fqdn_value->wifi = FALSE;
      } else {
         DBG("Connection %s is already disconnected", NAD_name);
         ret = FALSE;
         goto out;
      }
   } else {
      DBG("Wrong connection name\n");
      ret = FALSE;
      goto out;
   }


   DBG("Print states after update\n");
   fqdn_display_states(fqdn_value);
   ret = TRUE;

out:
   return ret;
}



bool fqdn_remote_ip_discard(struct vpn_provider *provider) {
   const char *host_fqdn = NULL;
   bool ret;
   host_fqdn = vpn_provider_get_string(provider, "Host");
   ret = g_hash_table_contains(conn_tracker, host_fqdn);
   if (ret == FALSE) {
      goto out;
   }
   DBG("Lookup remote_ip_discard\n");
   tracker_value *fqdn_value = g_hash_table_lookup(conn_tracker, host_fqdn);

   if (fqdn_value->remote_ip == NULL) {
      ret = FALSE;
      DBG("IP value is already NULL\n");
      goto out;
   }
   DBG("Print states at discard\n");
   fqdn_display_states(fqdn_value);
   if (fqdn_value->NAD0 == FALSE && fqdn_value->NAD1 == FALSE && fqdn_value->NAD2 == FALSE && fqdn_value->ethernet == FALSE && fqdn_value->wifi == FALSE) {
      DBG("All the connections are disconnected so discarding IP\n");
      g_hash_table_remove(conn_tracker, host_fqdn);
      ret = TRUE;
   } else {
      DBG("Connections are still present and IP not discarded\n");
      ret = FALSE;
   }
out:
   return ret;
}

static void fqdn_display_states(tracker_value *fqdn) {
   DBG("Remote IP: %s\n", fqdn->remote_ip);
   DBG("NAD0: %d\n", fqdn->NAD0);
   DBG("NAD1: %d\n", fqdn->NAD1);
   DBG("NAD2: %d\n", fqdn->NAD2);
   DBG("Ethernet: %d\n", fqdn->ethernet);
   DBG("Wifi: %d\n", fqdn->wifi);

}
