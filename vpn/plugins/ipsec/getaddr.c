/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.

 * * Neither the name of The Linux Foundation nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.

 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gdbus.h>
#include <errno.h>
#include <dbus/dbus.h>
#include "getaddr.h"
#include <vpn/vpn-provider.h>
#include <connman/log.h>

#define CONNMAN_DBUS_NAME "net.connman"
#define CONNMAN_MANAGER_PATH "/"
#define CONNMAN_MANAGER_INTERFACE CONNMAN_DBUS_NAME ".Manager"

static DBusConnection *dbus_conn;
static GHashTable *provider_hashlist;

typedef struct {
	char* ipaddress;
	char* service_keyname;
}dict_data;

typedef gboolean (*dbus_foreach_callback_f) (DBusMessageIter *iter,void **user_data);

static int dbus_get_basic(DBusMessageIter *iter,
			int dbus_type,
			void *dest)
{
	if (dest == NULL)
		return -EINVAL;

	if (dbus_message_iter_get_arg_type(iter) != dbus_type)
		return -EINVAL;

	dbus_message_iter_get_basic(iter, dest);

	return 0;
}

static void dbus_foreach_dict_entry(DBusMessageIter *iter,
					dbus_foreach_callback_f callback,
					void** userdata)
{
	DBusMessageIter array, dict_entry;

	if (dbus_message_iter_get_arg_type(iter)!= DBUS_TYPE_ARRAY) {
	    DBG("type is %c\n", dbus_message_iter_get_arg_type(iter));
	    DBG("dbus_foreach_dict_entry not array type");
	    goto out;
	}

	dbus_message_iter_recurse(iter, &array);
	DBG("before check dict entry %c\r\n", dbus_message_iter_get_arg_type(&array));
	while (dbus_message_iter_get_arg_type(&array) != DBUS_TYPE_INVALID)
	{
		DBG("dbus_foreach_dict_entry!\n");
		dbus_message_iter_recurse(&array, &dict_entry);
		if (callback(&dict_entry, userdata))
		{
			DBG("finded ----\r\n");
			break;
		}
		DBG("dbus_foreach_dict_entry exit! %s\n",*userdata);
		dbus_message_iter_next(&array);
	}
out:
	return;
}

static gboolean get_ipaddress(DBusMessageIter *arg, void** userdata)
{
	char* value;
	const char* name;
	dbus_message_iter_get_basic(arg, &name);

	DBG("get_ipaddress %s", name);
	if (0 == g_strcmp0(name, "Address")) {
		DBusMessageIter variant;
		const char* value;
		DBG("get_ipaddress obtain ipaddress before%s", name);
		dbus_message_iter_next(arg);
		dbus_message_iter_recurse(arg, &variant);
		dbus_message_iter_get_basic(&variant, &value);
		DBG("get_ipaddress obtain ipaddress after %s", value);

		if (value != NULL)
		{
			*userdata = g_strdup(value);
			DBG("get_ipaddress obtain ipaddress after %s", *userdata);
			return TRUE;
		}
	}
	return FALSE;
}
static gboolean obtain_ipaddress(DBusMessageIter *arg, void** userdata)
{
	DBusMessageIter dict;
	gboolean set = FALSE;
	char *value;

	DBG("enter obtain_ipaddress");
	dbus_message_iter_recurse(arg, &dict);
	dbus_foreach_dict_entry(&dict, get_ipaddress, userdata);

	if (*userdata != NULL)
		return TRUE;
	else
		return FALSE;
}

static gboolean parse_dict(DBusMessageIter *arg, void** userdata)
{
	char* ipaddress = NULL;
	const char* name;
	struct vpn_provider *provider = NULL;
	dict_data* pdata = (dict_data *)(*userdata);

	dbus_message_iter_get_basic(arg, &name);
	DBG("parse_dict %s\r\n", name);
	dbus_message_iter_next(arg);
	if (0 == g_strcmp0(name, "State")) {
		DBusMessageIter variant;
		const char* value;
		dbus_message_iter_recurse(arg, &variant);
		dbus_message_iter_get_basic(&variant, &value);
		/*
		 * if service state is not either ready or online
		 * then no ipaddress available, just return
		 */
		if (g_strcmp0(value, "ready") && g_strcmp0(value, "online"))
			return TRUE;
	}
	else if (0 == g_strcmp0(name, "Name")) {
		DBusMessageIter variant;
		const char* value;
		char* new_value;
		dbus_message_iter_recurse(arg, &variant);
		dbus_message_iter_get_basic(&variant, &value);

		DBG("Name is %s %d. %s\r\n", value, __LINE__, pdata->service_keyname);
		DBG("hash table size is %d\r\n", g_hash_table_size(provider_hashlist));

		provider = g_hash_table_lookup(provider_hashlist, pdata->service_keyname);

		if (provider && !g_str_match_string(value,
					vpn_provider_get_string(provider, "Name"), 0)) {
			new_value = g_strdup_printf("%s_%s",
					vpn_provider_get_string(provider, "Name"), value);
			DBG("new Name is %s\r\n", new_value);
			vpn_provider_set_string(provider, name, new_value);
		}
		DBG("new Name is %s\r\n", vpn_provider_get_string(provider, "Name"));
		return FALSE;
	}
	else if((0 == g_strcmp0(name, "IPv4")) || (0 == g_strcmp0(name, "IPv6")))
		return obtain_ipaddress(arg, (void**)(&(pdata->ipaddress)));
	else
		return FALSE;
}


char* parse_manager_msg(DBusMessage *msg, char* service_keyname)
{
	DBusMessageIter iter, array;
	char* name;
	dict_data*  pdata;

	pdata = g_try_new0(dict_data, 1);
	if (!pdata) {
		DBG("alloc dict_data failed");
		goto out;
	}

	pdata->ipaddress = NULL;
	pdata->service_keyname = service_keyname;
	dbus_message_iter_init(msg, &iter);
	if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_ARRAY) {
	    DBG("parse_manager_msg type is not array\r\n");
	    goto out;
	}

	dbus_message_iter_recurse(&iter, &array);

	while (dbus_message_iter_get_arg_type(&array) != DBUS_TYPE_INVALID) {
		DBusMessageIter entry;
		const char *obj_path;

		dbus_message_iter_recurse(&array, &entry);
		dbus_get_basic(&entry, DBUS_TYPE_OBJECT_PATH, &obj_path);
		//dbus_message_iter_get_basic(&entry, &obj_path);
		if (obj_path) {
			/*
			 * check if object path
			 * obj path is something like "/net/connman/service/wifi_xxx"
			 */
			char* obj_name = g_strrstr(obj_path, "/");
			DBG("obj_name: %s keyname: %s\n",obj_name, service_keyname);
			if (g_strrstr(obj_name + 1 ,service_keyname)!=NULL) {
				DBG("matches!\n");
				dbus_message_iter_next(&entry);
				dbus_foreach_dict_entry(&entry, parse_dict, (void**)&pdata);
				DBG("ipaddress %s from service keyname %s\n",pdata->ipaddress,service_keyname);
			}
		}
		DBG("ipaddress from obj name %s\n",pdata->ipaddress);
		if (pdata->ipaddress)
			break;
		dbus_message_iter_next(&array);
	}
out:
	if (pdata && pdata->ipaddress)
		return pdata->ipaddress;
	else
		return NULL;
}

/*
 * find the ipaddr for service key word
 * user has the responsibility of free(ipaddress)
 * success : return ipaddress
 * fail : return NULL
 */

char* find_ipaddress_by_service_keyname(char* service_keyname,
		struct vpn_provider* provider)
{
	DBusMessage *message, *reply;
	DBusError err;
	char* ipaddress = NULL;

	fprintf(stderr, "enter find_ipaddress_by_service_keyname\n");

	/*
	 * setup dbus connection
	 */
	dbus_error_init(&err);
	if (NULL == dbus_conn) {
		dbus_conn = g_dbus_setup_bus(DBUS_BUS_SYSTEM, NULL, &err);
		if (!dbus_conn) {
			if (dbus_error_is_set(&err)) {
				DBG("%s", err.message);
				dbus_error_free(&err);
			}
			else
				DBG("Can't register with system bus\n");
			return NULL;
		}

	}

	fprintf(stderr, "get dbus connection\n");
	message = dbus_message_new_method_call(CONNMAN_DBUS_NAME,
					CONNMAN_MANAGER_PATH,
					CONNMAN_MANAGER_INTERFACE,
					"GetServices");
	if (message == NULL)
		return NULL;

	fprintf(stderr, "get dbus message\n");
	reply = dbus_connection_send_with_reply_and_block(dbus_conn, message,
			DBUS_TIMEOUT_USE_DEFAULT, &err);

	dbus_message_unref(message);
	if (!reply) {
		if (dbus_error_is_set(&err)) {
			DBG("%s", err.message);
			dbus_error_free(&err);
		} else {
			DBG("Failed to get properties");
		}

		goto done;
	}

	/*
	 * make a hash table for provider
	 */
	if (!provider_hashlist)
		provider_hashlist = g_hash_table_new(g_str_hash, g_str_equal);

	DBG("hash table size is %d before service_key_name %s provider is %p\r\n",
						g_hash_table_size(provider_hashlist), service_keyname, provider);
	g_hash_table_insert(provider_hashlist, service_keyname, provider);
	DBG("hash table size is %d after \r\n", g_hash_table_size(provider_hashlist));

	fprintf(stderr, "start parsing\n");
	ipaddress = parse_manager_msg(reply, service_keyname);

done:
	dbus_message_unref(reply);
	return ipaddress;
}
