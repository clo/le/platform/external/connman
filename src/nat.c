/*
 *
 *  Connection Manager
 *
 *  Copyright (C) 2007-2012  Intel Corporation. All rights reserved.
 *  Copyright (C) 2012-2014  BMW Car IT GmbH.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <stdio.h>

#include "connman.h"

static char *default_interface;
static GHashTable *nat_hash;

struct connman_nat {
	char *address;
	unsigned char prefixlen;
	struct firewall_context *fw;

	char *interface;
	char **pre_nat_interface;
};

/*
 * ConnMan will create MASQUERADE rules for all interfaces which is included
 * in "TetheringInterfaceNAT" at first.
 */
static bool has_pre_nat()
{
	char** nat_interface;

	nat_interface = connman_setting_get_string_list("TetheringInterfaceNAT");
	if (!nat_interface)
		return false;

	return true;
}

static int enable_all_pre_nat(struct connman_nat *nat)
{
	char *cmd;
	int err;
	char** nat_interface;

	DBG("");
	nat_interface = connman_setting_get_string_list("TetheringInterfaceNAT");
	if (!nat_interface)
		return 0;

	nat->pre_nat_interface = nat_interface;

	int indx = 0;
	bool need_nat_rule = false;
	bool has_rule_success = false;
	while (nat->pre_nat_interface[indx]) {
		DBG("enable interface %s at %d", nat->pre_nat_interface[indx], indx);
		if (NULL != __connman_tethering_lookup_eth_iface_config(nat->pre_nat_interface[indx])) {
			connman_warn("%s: WARN : %s is exist in tehter config",
					__func__, nat->pre_nat_interface[indx]);
			indx++;
			continue;
		}
		need_nat_rule = true;
		/* Enable masquerading */
		cmd = g_strdup_printf("-s %s/%d -o %s -j MASQUERADE",
						nat->address,
						nat->prefixlen,
						nat->pre_nat_interface[indx]);

		err = __connman_firewall_add_rule(nat->fw, "nat",
					"POSTROUTING", cmd);
		g_free(cmd);
		if (err < 0) {
			connman_warn("%s: WARN : failed to add rule %s at %d",
					__func__, nat->pre_nat_interface[indx], indx);
		} else {
			has_rule_success = true;
		}
		indx++;
	}

	if (need_nat_rule && !has_rule_success)
		return err;

	return __connman_firewall_enable(nat->fw);
}

static void disable_all_pre_nat(struct connman_nat *nat)
{
	DBG("disable interface");
	/* Disable masquerading */
	__connman_firewall_disable(nat->fw);
	__connman_firewall_remove_all_rule(nat->fw);
}

static int enable_ip_forward(bool enable)
{
	FILE *f;

	f = fopen("/proc/sys/net/ipv4/ip_forward", "r+");
	if (!f)
		return -errno;

	if (enable)
		fprintf(f, "1");
	else
		fprintf(f, "0");

	fclose(f);

	return 0;
}

static int enable_nat(struct connman_nat *nat)
{
	char *cmd;
	int err;

	g_free(nat->interface);
	nat->interface = g_strdup(default_interface);

	if (!nat->interface)
		return 0;

	DBG("enable interface %s", nat->interface);
	/* Enable masquerading */
	cmd = g_strdup_printf("-s %s/%d -o %s -j MASQUERADE",
					nat->address,
					nat->prefixlen,
					nat->interface);

	err = __connman_firewall_add_rule(nat->fw, "nat",
				"POSTROUTING", cmd);
	g_free(cmd);
	if (err < 0)
		return err;

	return __connman_firewall_enable(nat->fw);
}

static void disable_nat(struct connman_nat *nat)
{
	if (!nat->interface)
		return;

	DBG("disable interface %s", nat->interface);
	/* Disable masquerading */
	__connman_firewall_disable(nat->fw);
	__connman_firewall_remove_all_rule(nat->fw);
}

int __connman_nat_enable(const char *name, const char *address,
				unsigned char prefixlen)
{
	struct connman_nat *nat;
	int err;

	if (g_hash_table_size(nat_hash) == 0) {
		err = enable_ip_forward(true);
		if (err < 0)
			return err;
	}

	nat = g_try_new0(struct connman_nat, 1);
	if (!nat)
		goto err;

	nat->fw = __connman_firewall_create();
	if (!nat->fw)
		goto err;

	nat->address = g_strdup(address);
	nat->prefixlen = prefixlen;

	g_hash_table_replace(nat_hash, g_strdup(name), nat);

	if (has_pre_nat())
		return enable_all_pre_nat(nat);
	else
		return enable_nat(nat);

err:
	if (nat) {
		if (nat->fw)
			__connman_firewall_destroy(nat->fw);
		g_free(nat);
	}

	if (g_hash_table_size(nat_hash) == 0)
		enable_ip_forward(false);

	return -ENOMEM;
}

void __connman_nat_disable(const char *name)
{
	struct connman_nat *nat;

	nat = g_hash_table_lookup(nat_hash, name);
	if (!nat)
		return;

	if (has_pre_nat())
		disable_all_pre_nat(nat);
	else
		disable_nat(nat);

	g_hash_table_remove(nat_hash, name);

	if (g_hash_table_size(nat_hash) == 0)
		enable_ip_forward(false);
}

static void update_default_interface(struct connman_service *service)
{
	GHashTableIter iter;
	gpointer key, value;
	char *interface;
	int err;

	interface = connman_service_get_interface(service);

	DBG("interface %s", interface);

	if (has_pre_nat()) {
		connman_info("%s: no action in pre nat mode", __func__);
		return;
	}

	enable_ip_forward(false);

	g_free(default_interface);
	default_interface = interface;

	g_hash_table_iter_init(&iter, nat_hash);

	while (g_hash_table_iter_next(&iter, &key, &value)) {
		const char *name = key;
		struct connman_nat *nat = value;

		disable_nat(nat);
		err = enable_nat(nat);
		if (err < 0)
			DBG("Failed to enable nat for %s", name);
	}

	if (interface) {
		connman_info("enable ip_forward");
		enable_ip_forward(true);
	}

}

static void shutdown_nat(gpointer key, gpointer value, gpointer user_data)
{
	const char *name = key;

	__connman_nat_disable(name);
}

static void cleanup_nat(gpointer data)
{
	struct connman_nat *nat = data;

	__connman_firewall_destroy(nat->fw);
	nat->pre_nat_interface = NULL;
	g_free(nat->address);
	g_free(nat->interface);
	g_free(nat);
}

static struct connman_notifier nat_notifier = {
	.name			= "nat",
	.default_changed	= update_default_interface,
};

int __connman_nat_init(void)
{
	int err;

	DBG("");

	err = connman_notifier_register(&nat_notifier);
	if (err < 0)
		return err;

	nat_hash = g_hash_table_new_full(g_str_hash, g_str_equal,
						g_free, cleanup_nat);

	return 0;
}

void __connman_nat_cleanup(void)
{
	DBG("");

	g_hash_table_foreach(nat_hash, shutdown_nat, NULL);
	g_hash_table_destroy(nat_hash);
	nat_hash = NULL;

	connman_notifier_unregister(&nat_notifier);
}
