/*
 *
 *  Connection Manager
 *
 *  Copyright (C) 2007-2013  Intel Corporation. All rights reserved.
 *  Copyright (C) 2011	ProFUSION embedded systems
 *  Copyright (c) 2017, The Linux Foundation. All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <string.h>
#include <fcntl.h>
#include <linux/if_tun.h>
#include <netinet/in.h>
#include <linux/if_bridge.h>

#include <stdlib.h>

#include "connman.h"

#include <gdhcp/gdhcp.h>

#include <gdbus.h>
#include <netdb.h>

#ifndef DBUS_TYPE_UNIX_FD
#define DBUS_TYPE_UNIX_FD -1
#endif

#define BRIDGE_NAME "tether"

#define TETHER_ETH_CONFIG_FILE "tether_ethernet"

#define DEFAULT_MTU	1500

#define SUBNET_MASK_24_ADDRESS	"255.255.255.0"

static char *private_network_primary_dns = NULL;
static char *private_network_secondary_dns = NULL;

static volatile int tethering_enabled;
static bool tethering_config_enabled;
static GDHCPServer *tethering_dhcp_server = NULL;
static struct connman_ippool *dhcp_ippool = NULL;
static DBusConnection *connection;
static GHashTable *pn_hash;
static GHashTable *tether_hash;

struct connman_private_network {
	char *owner;
	char *path;
	guint watch;
	DBusMessage *msg;
	DBusMessage *reply;
	int fd;
	char *interface;
	int index;
	guint iface_watch;
	struct connman_ippool *pool;
	char *primary_dns;
	char *secondary_dns;
};

struct connman_tether_config {
	char *name;
	enum connman_service_type type;
	char *description;
	GHashTable *tether_iface_hash;
};

struct connman_tether_interface {
	char *name;
	bool is_found;	// it is used for auto tethering
};

struct connman_tether_iface_config {
	char *tether_name;
	char *host;
	char *netmask;

	char *interfaces;
	GList* interfaces_list;

	char *client;
	char *dhcp;
	char *dhcp_start;
	char *dhcp_end;
	struct connman_ippool *ippool;

	GHashTable *setting_strings;

	int type;
	int bridge_index;
	GDHCPServer* tethering_dhcp_server;
};

static bool connman_tethering_iface_is_tethered(struct connman_tether_iface_config* iface_config);
static void connman_tethering_iface_config_disable(struct connman_tether_config* config);
static bool connman_tethering_iface_config_has_tether_iface(struct connman_tether_config* config);
static void connman_tethering_config_disable();
static bool connman_tethering_config_has_tether_iface();

const char *__connman_tethering_get_bridge(void)
{
	int sk, err;
	unsigned long args[3];

	sk = socket(AF_INET, SOCK_STREAM, 0);
	if (sk < 0)
		return NULL;

	args[0] = BRCTL_GET_VERSION;
	args[1] = args[2] = 0;
	err = ioctl(sk, SIOCGIFBR, &args);
	close(sk);
	if (err == -1) {
		connman_error("Missing support for 802.1d ethernet bridging");
		return NULL;
	}

	return BRIDGE_NAME;
}

static void dhcp_server_debug(const char *str, void *data)
{
	connman_info("%s: %s\n", (const char *) data, str);
}

static void dhcp_server_error(GDHCPServerError error)
{
	switch (error) {
	case G_DHCP_SERVER_ERROR_NONE:
		connman_error("OK");
		break;
	case G_DHCP_SERVER_ERROR_INTERFACE_UNAVAILABLE:
		connman_error("Interface unavailable");
		break;
	case G_DHCP_SERVER_ERROR_INTERFACE_IN_USE:
		connman_error("Interface in use");
		break;
	case G_DHCP_SERVER_ERROR_INTERFACE_DOWN:
		connman_error("Interface down");
		break;
	case G_DHCP_SERVER_ERROR_NOMEM:
		connman_error("No memory");
		break;
	case G_DHCP_SERVER_ERROR_INVALID_INDEX:
		connman_error("Invalid index");
		break;
	case G_DHCP_SERVER_ERROR_INVALID_OPTION:
		connman_error("Invalid option");
		break;
	case G_DHCP_SERVER_ERROR_IP_ADDRESS_INVALID:
		connman_error("Invalid address");
		break;
	}
}

static GDHCPServer *dhcp_server_start(const char *bridge,
				const char *router, const char *subnet,
				const char *start_ip, const char *end_ip,
				unsigned int lease_time, const char *dns)
{
	GDHCPServerError error;
	GDHCPServer *dhcp_server;
	int index;

	DBG("");

	index = connman_inet_ifindex(bridge);
	if (index < 0)
		return NULL;

	dhcp_server = g_dhcp_server_new(G_DHCP_IPV4, index, &error);
	if (!dhcp_server) {
		dhcp_server_error(error);
		return NULL;
	}

	g_dhcp_server_set_debug(dhcp_server, dhcp_server_debug, "DHCP server");

	g_dhcp_server_set_lease_time(dhcp_server, lease_time);
	g_dhcp_server_set_option(dhcp_server, G_DHCP_SUBNET, subnet);
	g_dhcp_server_set_option(dhcp_server, G_DHCP_ROUTER, router);
	g_dhcp_server_set_option(dhcp_server, G_DHCP_DNS_SERVER, dns);
	g_dhcp_server_set_ip_range(dhcp_server, start_ip, end_ip);

	g_dhcp_server_start(dhcp_server);

	return dhcp_server;
}

static void dhcp_server_stop(GDHCPServer *server)
{
	if (!server)
		return;

	g_dhcp_server_unref(server);
}

static bool __connman_tethering_set_index(struct connman_tether_iface_config* iface_config, int index)
{
	if (NULL == iface_config) {
		connman_error("%s: ERR : iface is NULL", __func__);
		return false;
	}
	iface_config->bridge_index = index;
	return true;
}

static bool __connman_tethering_update_index(struct connman_tether_iface_config* iface_config, int index)
{
	if (NULL == iface_config || NULL == iface_config->ippool) {
		connman_error("%s: ERR : iface is NULL", __func__);
		return false;
	}
	if (!__connman_tethering_set_index(iface_config, index)) {
		return false;
	}
	if (!__connman_ippool_set_index(iface_config->ippool, index)) {
		return false;
	}
	return true;
}

static void tethering_restart(struct connman_ippool *pool, void *user_data)
{
	DBG("pool %p", pool);
	__connman_tethering_set_disabled();
	__connman_tethering_set_enabled();
}

static void tethering_ip_pool_conflict(struct connman_ippool *pool, void *user_data)
{
	connman_error("%s: ERR : ip pool has conflict gateway %s",
				__func__, __connman_ippool_get_gateway(pool));
}

void __connman_tethering_eth_set_enabled(struct connman_tether_iface_config* iface_config)
{
	int index;
	int err;
	char *cmd;
	const char *gateway;
	const char *broadcast;
	const char *subnet_mask;
	const char *start_ip;
	const char *end_ip;
	const char *dns;
	unsigned char prefixlen;
	char **ns;
	char* bridge_name;

	if (NULL == iface_config || NULL == iface_config->tether_name || NULL == iface_config->ippool) {
		connman_error("%s: ERR : nullpointer", __func__);
		return;
	}
	bridge_name = iface_config->tether_name;

	DBG("bridge %s", bridge_name);

	if (connman_tethering_iface_is_tethered(iface_config)) {
		connman_warn("%s: WARN : %s is already tethered", __func__, bridge_name);
		return;
	}

	err = __connman_bridge_create(bridge_name);
	if (err < 0) {
		return;
	}
	__connman_tethering_set_index(iface_config, index);

	index = connman_inet_ifindex(bridge_name);

	gateway = __connman_ippool_get_gateway(iface_config->ippool);
	broadcast = __connman_ippool_get_broadcast(iface_config->ippool);
	subnet_mask = __connman_ippool_get_subnet_mask(iface_config->ippool);
	start_ip = __connman_ippool_get_start_ip(iface_config->ippool);
	end_ip = __connman_ippool_get_end_ip(iface_config->ippool);

	err = __connman_bridge_enable(bridge_name, gateway,
				connman_ipaddress_calc_netmask_len(subnet_mask),
				broadcast);
	if (err < 0 && err != -EALREADY) {
		__connman_bridge_remove(bridge_name);
		__connman_tethering_set_index(iface_config, -1);
		return;
	}

	ns = connman_setting_get_string_list("FallbackNameservers");
	if (ns) {
		if (ns[0]) {
			g_free(private_network_primary_dns);
			private_network_primary_dns = g_strdup(ns[0]);
		}
		if (ns[1]) {
			g_free(private_network_secondary_dns);
			private_network_secondary_dns = g_strdup(ns[1]);
		}

		DBG("Fallback ns primary %s secondary %s",
			private_network_primary_dns,
			private_network_secondary_dns);
	}

	dns = gateway;
	if (__connman_dnsproxy_add_listener(index) < 0) {
		connman_error("Can't add listener %s to DNS proxy",bridge_name);
		dns = private_network_primary_dns;
		DBG("Serving %s nameserver to clients", dns);
	}

	iface_config->tethering_dhcp_server = dhcp_server_start(bridge_name,
										gateway, subnet_mask,
										start_ip, end_ip,
										24 * 3600, dns);
	if (!iface_config->tethering_dhcp_server) {
		__connman_bridge_disable(bridge_name);
		__connman_bridge_remove(bridge_name);
		__connman_tethering_set_index(iface_config, -1);
		return;
	}

	prefixlen = connman_ipaddress_calc_netmask_len(subnet_mask);
	err = __connman_nat_enable(bridge_name, start_ip, prefixlen);
	if (err < 0) {
		connman_error("Cannot enable NAT %d/%s", err, strerror(-err));
		dhcp_server_stop(iface_config->tethering_dhcp_server);
		iface_config->tethering_dhcp_server = NULL;
		__connman_bridge_disable(bridge_name);
		__connman_bridge_remove(bridge_name);
		__connman_tethering_set_index(iface_config, -1);
		return;
	}
	tethering_config_enabled = true;
	__connman_tethering_update_index(iface_config, index);


	/* Add "iptables -t filter -A FORWARD -i <tether1/2> -p tcp -m state --state INVALID -j DROP" */

	/* Delete first and add back */

	cmd = g_strdup_printf("iptables -t filter -D FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",
							bridge_name);

	system(cmd);

	g_free(cmd);


	cmd = g_strdup_printf("iptables -t filter -A FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",
							bridge_name);

	system(cmd);

	g_free(cmd);

	DBG("tethering started");
}

void __connman_tethering_set_enabled(void)
{
	int index;
	int err;
	const char *gateway;
	const char *broadcast;
	const char *subnet_mask;
	const char *start_ip;
	const char *end_ip;
	const char *dns;
	unsigned char prefixlen;
	char **ns;
	char *cmd;

	DBG("enabled %d", tethering_enabled + 1);

	if (__sync_fetch_and_add(&tethering_enabled, 1) != 0)
		return;

	err = __connman_bridge_create(BRIDGE_NAME);
	if (err < 0) {
		__sync_fetch_and_sub(&tethering_enabled, 1);
		return;
	}

	index = connman_inet_ifindex(BRIDGE_NAME);
	dhcp_ippool = __connman_ippool_create(index, 2, 252,
						tethering_restart, NULL);
	if (!dhcp_ippool) {
		connman_error("Fail to create IP pool");
		__connman_bridge_remove(BRIDGE_NAME);
		__sync_fetch_and_sub(&tethering_enabled, 1);
		return;
	}

	gateway = __connman_ippool_get_gateway(dhcp_ippool);
	broadcast = __connman_ippool_get_broadcast(dhcp_ippool);
	subnet_mask = __connman_ippool_get_subnet_mask(dhcp_ippool);
	start_ip = __connman_ippool_get_start_ip(dhcp_ippool);
	end_ip = __connman_ippool_get_end_ip(dhcp_ippool);

	err = __connman_bridge_enable(BRIDGE_NAME, gateway,
			connman_ipaddress_calc_netmask_len(subnet_mask),
			broadcast);
	if (err < 0 && err != -EALREADY) {
		__connman_ippool_unref(dhcp_ippool);
		__connman_bridge_remove(BRIDGE_NAME);
		__sync_fetch_and_sub(&tethering_enabled, 1);
		return;
	}

	ns = connman_setting_get_string_list("FallbackNameservers");
	if (ns) {
		if (ns[0]) {
			g_free(private_network_primary_dns);
			private_network_primary_dns = g_strdup(ns[0]);
		}
		if (ns[1]) {
			g_free(private_network_secondary_dns);
			private_network_secondary_dns = g_strdup(ns[1]);
		}

		DBG("Fallback ns primary %s secondary %s",
			private_network_primary_dns,
			private_network_secondary_dns);
	}

	dns = gateway;
	if (__connman_dnsproxy_add_listener(index) < 0) {
		connman_error("Can't add listener %s to DNS proxy",
								BRIDGE_NAME);
		dns = private_network_primary_dns;
		DBG("Serving %s nameserver to clients", dns);
	}

	tethering_dhcp_server = dhcp_server_start(BRIDGE_NAME,
						gateway, subnet_mask,
						start_ip, end_ip,
						24 * 3600, dns);
	if (!tethering_dhcp_server) {
		__connman_bridge_disable(BRIDGE_NAME);
		__connman_ippool_unref(dhcp_ippool);
		__connman_bridge_remove(BRIDGE_NAME);
		__sync_fetch_and_sub(&tethering_enabled, 1);
		return;
	}

	prefixlen = connman_ipaddress_calc_netmask_len(subnet_mask);
	err = __connman_nat_enable(BRIDGE_NAME, start_ip, prefixlen);
	if (err < 0) {
		connman_error("Cannot enable NAT %d/%s", err, strerror(-err));
		dhcp_server_stop(tethering_dhcp_server);
		__connman_bridge_disable(BRIDGE_NAME);
		__connman_ippool_unref(dhcp_ippool);
		__connman_bridge_remove(BRIDGE_NAME);
		__sync_fetch_and_sub(&tethering_enabled, 1);
		return;
	}

	err = __connman_ipv6pd_setup(BRIDGE_NAME);
	if (err < 0 && err != -EINPROGRESS)
		DBG("Cannot setup IPv6 prefix delegation %d/%s", err,
			strerror(-err));

	cmd = g_strdup_printf("iptables -t filter -D FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",
						BRIDGE_NAME);
	system(cmd);
	g_free(cmd);
	cmd = g_strdup_printf("iptables -t filter -A FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",						BRIDGE_NAME);
	system(cmd);
	g_free(cmd);
	DBG("tethering started");
}

void __connman_tethering_eth_set_disabled(struct connman_tether_iface_config* iface_config)
{
	int index;
	char *cmd;

	DBG("");

	if (NULL == iface_config || NULL == iface_config->tether_name) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return;
	}

	if (!connman_tethering_iface_is_tethered(iface_config)) {
		connman_warn("%s : WARN : %s is already disabled", __func__, iface_config->tether_name);
		if (!connman_tethering_config_has_tether_iface()) {
			connman_warn("%s : WARN : all tether iface is already ", __func__);
			tethering_config_enabled = false;
		}
		return;
	}

	connman_info("%s: do cleanup", __func__);

	index = connman_inet_ifindex(iface_config->tether_name);
	__connman_dnsproxy_remove_listener(index);
	__connman_nat_disable(iface_config->tether_name);

	dhcp_server_stop(iface_config->tethering_dhcp_server);

	iface_config->tethering_dhcp_server = NULL;

	__connman_bridge_disable(iface_config->tether_name);

	__connman_bridge_remove(iface_config->tether_name);

	g_free(private_network_primary_dns);
	private_network_primary_dns = NULL;
	g_free(private_network_secondary_dns);
	private_network_secondary_dns = NULL;

	__connman_tethering_update_index(iface_config, -1);

	/* Disable "iptables -t filter -A FORWARD -i <tether1/2> -p tcp -m state --state INVALID -j DROP" */

	cmd = g_strdup_printf("iptables -t filter -D FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",
							iface_config->tether_name);

	system(cmd);

	g_free(cmd);


	if (!connman_tethering_config_has_tether_iface()) {
		tethering_config_enabled = false;
	}
	DBG("tethering stopped : config_enabled = %d ",tethering_config_enabled);
}

void __connman_tethering_set_disabled(void)
{
	int index;
	char *cmd;

	DBG("enabled %d", tethering_enabled - 1);

	if (__sync_fetch_and_sub(&tethering_enabled, 1) != 1)
		return;

	__connman_ipv6pd_cleanup();

	index = connman_inet_ifindex(BRIDGE_NAME);
	__connman_dnsproxy_remove_listener(index);

	__connman_nat_disable(BRIDGE_NAME);

	dhcp_server_stop(tethering_dhcp_server);

	tethering_dhcp_server = NULL;

	__connman_bridge_disable(BRIDGE_NAME);

	__connman_ippool_unref(dhcp_ippool);

	__connman_bridge_remove(BRIDGE_NAME);

	g_free(private_network_primary_dns);
	private_network_primary_dns = NULL;
	g_free(private_network_secondary_dns);
	private_network_secondary_dns = NULL;

	cmd = g_strdup_printf("iptables -t filter -D FORWARD -i %s -p tcp -m state --state INVALID -j DROP -w",				BRIDGE_NAME);
	system(cmd);
	g_free(cmd);
	DBG("tethering stopped");
}

static void setup_tun_interface(unsigned int flags, unsigned change,
		void *data)
{
	struct connman_private_network *pn = data;
	unsigned char prefixlen;
	DBusMessageIter array, dict;
	const char *server_ip;
	const char *peer_ip;
	const char *subnet_mask;
	int err;

	DBG("index %d flags %d change %d", pn->index,  flags, change);

	if (flags & IFF_UP)
		return;

	subnet_mask = __connman_ippool_get_subnet_mask(pn->pool);
	server_ip = __connman_ippool_get_start_ip(pn->pool);
	peer_ip = __connman_ippool_get_end_ip(pn->pool);
	prefixlen = connman_ipaddress_calc_netmask_len(subnet_mask);

	if ((__connman_inet_modify_address(RTM_NEWADDR,
				NLM_F_REPLACE | NLM_F_ACK, pn->index, AF_INET,
				server_ip, peer_ip, prefixlen, NULL)) < 0) {
		DBG("address setting failed");
		return;
	}

	connman_inet_ifup(pn->index);

	err = __connman_nat_enable(BRIDGE_NAME, server_ip, prefixlen);
	if (err < 0) {
		connman_error("failed to enable NAT");
		goto error;
	}

	dbus_message_iter_init_append(pn->reply, &array);

	dbus_message_iter_append_basic(&array, DBUS_TYPE_OBJECT_PATH,
						&pn->path);

	connman_dbus_dict_open(&array, &dict);

	connman_dbus_dict_append_basic(&dict, "ServerIPv4",
					DBUS_TYPE_STRING, &server_ip);
	connman_dbus_dict_append_basic(&dict, "PeerIPv4",
					DBUS_TYPE_STRING, &peer_ip);
	if (pn->primary_dns)
		connman_dbus_dict_append_basic(&dict, "PrimaryDNS",
					DBUS_TYPE_STRING, &pn->primary_dns);

	if (pn->secondary_dns)
		connman_dbus_dict_append_basic(&dict, "SecondaryDNS",
					DBUS_TYPE_STRING, &pn->secondary_dns);

	connman_dbus_dict_close(&array, &dict);

	dbus_message_iter_append_basic(&array, DBUS_TYPE_UNIX_FD, &pn->fd);

	g_dbus_send_message(connection, pn->reply);

	return;

error:
	pn->reply = __connman_error_failed(pn->msg, -err);
	g_dbus_send_message(connection, pn->reply);

	g_hash_table_remove(pn_hash, pn->path);
}

static void remove_private_network(gpointer user_data)
{
	struct connman_private_network *pn = user_data;

	__connman_nat_disable(BRIDGE_NAME);
	connman_rtnl_remove_watch(pn->iface_watch);
	__connman_ippool_unref(pn->pool);

	if (pn->watch > 0) {
		g_dbus_remove_watch(connection, pn->watch);
		pn->watch = 0;
	}
	close(pn->fd);

	g_free(pn->interface);
	g_free(pn->owner);
	g_free(pn->path);
	g_free(pn->primary_dns);
	g_free(pn->secondary_dns);
	g_free(pn);
}

static void owner_disconnect(DBusConnection *conn, void *user_data)
{
	struct connman_private_network *pn = user_data;

	DBG("%s died", pn->owner);

	pn->watch = 0;

	g_hash_table_remove(pn_hash, pn->path);
}

static void ippool_disconnect(struct connman_ippool *pool, void *user_data)
{
	struct connman_private_network *pn = user_data;

	DBG("block used externally");

	g_hash_table_remove(pn_hash, pn->path);
}

int __connman_private_network_request(DBusMessage *msg, const char *owner)
{
	struct connman_private_network *pn;
	char *iface = NULL;
	char *path = NULL;
	int index, fd, err;

	if (DBUS_TYPE_UNIX_FD < 0)
		return -EINVAL;

	fd = connman_inet_create_tunnel(&iface);
	if (fd < 0)
		return fd;

	path = g_strdup_printf("/tethering/%s", iface);

	pn = g_hash_table_lookup(pn_hash, path);
	if (pn) {
		g_free(path);
		g_free(iface);
		close(fd);
		return -EEXIST;
	}

	index = connman_inet_ifindex(iface);
	if (index < 0) {
		err = -ENODEV;
		goto error;
	}
	DBG("interface %s", iface);

	err = connman_inet_set_mtu(index, DEFAULT_MTU);

	pn = g_try_new0(struct connman_private_network, 1);
	if (!pn) {
		err = -ENOMEM;
		goto error;
	}

	pn->owner = g_strdup(owner);
	pn->path = path;
	pn->watch = g_dbus_add_disconnect_watch(connection, pn->owner,
					owner_disconnect, pn, NULL);
	pn->msg = msg;
	pn->reply = dbus_message_new_method_return(pn->msg);
	if (!pn->reply)
		goto error;

	pn->fd = fd;
	pn->interface = iface;
	pn->index = index;
	pn->pool = __connman_ippool_create(pn->index, 1, 1, ippool_disconnect, pn);
	if (!pn->pool) {
		errno = -ENOMEM;
		goto error;
	}

	pn->primary_dns = g_strdup(private_network_primary_dns);
	pn->secondary_dns = g_strdup(private_network_secondary_dns);

	pn->iface_watch = connman_rtnl_add_newlink_watch(index,
						setup_tun_interface, pn);

	g_hash_table_insert(pn_hash, pn->path, pn);

	return 0;

error:
	close(fd);
	g_free(iface);
	g_free(path);
	if (pn)
		g_free(pn->owner);
	g_free(pn);
	return err;
}

int __connman_private_network_release(const char *path)
{
	struct connman_private_network *pn;

	pn = g_hash_table_lookup(pn_hash, path);
	if (!pn)
		return -EACCES;

	g_hash_table_remove(pn_hash, path);
	return 0;
}

static struct connman_tether_iface_config* 
connman_tethering_lookup_iface_config(struct connman_tether_config* config, const char* ifname)
{
	static GHashTableIter iter;
	static int len = 0;
	gpointer key, value;

	DBG("");
	if (NULL == config || NULL == ifname) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return NULL;
	}

	g_hash_table_iter_init(&iter, config->tether_iface_hash);
	len = strlen(ifname);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_iface_config *iface_config = value;
		if (NULL == iface_config) {
			connman_warn("%s: WARN : NULL pointer", __func__);
			continue;
		}
		GList *list;
		for (list = iface_config->interfaces_list; list; list = list->next) {
			struct connman_tether_interface* iface = list->data;
			if (0 == strncmp(ifname, iface->name, len))
				return iface_config;
		}
	}

	DBG("not found %s", ifname);
	return NULL;
}

bool __connman_tethering_all_eth_iface_is_up()
{
	static GHashTableIter iter;
	gpointer key, value;
	struct connman_tether_config* config = NULL;

	DBG("");
	config = g_hash_table_lookup(tether_hash,  GINT_TO_POINTER(CONNMAN_SERVICE_TYPE_ETHERNET));
	g_hash_table_iter_init(&iter, config->tether_iface_hash);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_iface_config *iface_config = value;
		if (NULL == iface_config) {
			connman_warn("%s: WARN : NULL pointer", __func__);
			continue;
		}
		GList *list;
		for (list = iface_config->interfaces_list; list; list = list->next) {
			struct connman_tether_interface* iface = list->data;
			if (!iface->is_found)
				return false;
		}
	}

	return true;
}

struct connman_tether_iface_config*
__connman_tethering_lookup_eth_iface_config(const char* ifname)
{
	struct connman_tether_config* config = NULL;

	DBG("");
	if (NULL == ifname) {
		connman_error("%s: ERR : NULL pointer ", __func__);
		return NULL;
	}

	config = g_hash_table_lookup(tether_hash,  GINT_TO_POINTER(CONNMAN_SERVICE_TYPE_ETHERNET));
	if (NULL != config) {
		return connman_tethering_lookup_iface_config(config, ifname);
	}
	connman_warn("%s: WARN : not ethernet iface ", __func__);
	return NULL;
}

static void set_interface_found(struct connman_tether_config* config, const char* ifname)
{
	static GHashTableIter iter;
	static int len = 0;
	gpointer key, value;

	DBG("");
	if (NULL == config || NULL == ifname) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return;
	}

	g_hash_table_iter_init(&iter, config->tether_iface_hash);
	len = strlen(ifname);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_iface_config *iface_config = value;
		if (NULL == iface_config) {
			connman_warn("%s: WARN : NULL pointer", __func__);
			continue;
		}
		DBG("%s: search %s", __func__, iface_config->tether_name);
		GList *list;
		for (list = iface_config->interfaces_list; list; list = list->next) {
			struct connman_tether_interface* iface = list->data;
			if (0 == strncmp(ifname, iface->name, len)) {
				connman_info("%s: found %s %s", __func__, iface_config->tether_name, iface->name);
				iface->is_found = true;
			}
		}
	}

	return;
}

void __connman_tethering_set_interface_found(enum connman_service_type type, const char* ifname)
{
	struct connman_tether_config* config = NULL;

	DBG("");
	if (NULL == ifname) {
		connman_error("%s: ERR : NULL pointer ", __func__);
		return;
	}

	config = g_hash_table_lookup(tether_hash, GINT_TO_POINTER(type));
	if (NULL == config) {
		connman_warn("%s: ERR : not ethernet iface ", __func__);
		return;
	}
	set_interface_found(config, ifname);
}

static void remove_interface(gpointer user_data)
{
	struct connman_tether_interface *iface = user_data;
	if (NULL == iface) {
		connman_warn("%s : WARN : iface is NULL", __func__);
		return;
	}
	iface->is_found = false;
	g_free(iface->name);
}

static bool connman_tethering_config_has_tether_iface()
{
	struct connman_tether_config* config = NULL;
	static GHashTableIter iter;
	gpointer key, value;
	gpointer item;

	DBG("");
	g_hash_table_iter_init(&iter, tether_hash);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_config *config = value;
		if (NULL == config) {
			connman_error("%s: ERR : NULL pointer", __func__);
			continue;
		}
		if (true == connman_tethering_iface_config_has_tether_iface(config)) {
			return true;
		}
	}

	return false;
}

static bool connman_tethering_iface_is_tethered(struct connman_tether_iface_config* iface_config)
{
	if (NULL == iface_config || -1 == iface_config->bridge_index)
		return false;
	return true;
}

static void connman_tethering_iface_config_disable(struct connman_tether_config* config)
{
	static GHashTableIter iter;
	gpointer key, value;

	DBG("");
	g_hash_table_iter_init(&iter, config->tether_iface_hash);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_iface_config *iface_config = value;
		if (NULL == iface_config) {
			connman_error("%s: ERR : NULL pointer", __func__);
			continue;
		}
		if (connman_tethering_iface_is_tethered(iface_config)) {
			DBG("disable index %d ifname %s", iface_config->bridge_index, iface_config->tether_name);
			if (iface_config->tethering_dhcp_server)
				dhcp_server_stop(iface_config->tethering_dhcp_server);
			__connman_bridge_disable(iface_config->tether_name);
			__connman_bridge_remove(iface_config->tether_name);
			__connman_nat_disable(iface_config->tether_name);
			__connman_tethering_update_index(iface_config, -1);
		}
	}

	return;
}

static bool connman_tethering_iface_config_has_tether_iface(struct connman_tether_config* config)
{
	static GHashTableIter iter;
	gpointer key, value;

	DBG("");
	g_hash_table_iter_init(&iter, config->tether_iface_hash);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_iface_config *iface_config = value;
		if (NULL == iface_config) {
			connman_error("%s: ERR : NULL pointer", __func__);
			continue;
		}
		if (connman_tethering_iface_is_tethered(iface_config)) {
			DBG("disable index %d ifname %s", iface_config->bridge_index, iface_config->tether_name);
			return true;
		}
	}

	return false;
}

static void remove_iface_config(gpointer user_data)
{

	struct connman_tether_iface_config *iface_config = user_data;
	DBG("");

	if (NULL == iface_config) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return;
	}
	if (connman_tethering_iface_is_tethered(iface_config)) {
		connman_warn("%s: WARN : cleanup index %d ifname %s",
					__func__, iface_config->bridge_index, iface_config->tether_name);
		if (iface_config->tethering_dhcp_server)
			dhcp_server_stop(iface_config->tethering_dhcp_server);
		__connman_bridge_disable(iface_config->tether_name);
		__connman_bridge_remove(iface_config->tether_name);
		__connman_nat_disable(iface_config->tether_name);
		__connman_tethering_update_index(iface_config, -1);
	}
	g_free(iface_config->tether_name);
	g_free(iface_config->interfaces);
	g_free(iface_config->host);
	g_free(iface_config->netmask);
	g_free(iface_config->client);
	g_free(iface_config->dhcp);
	g_free(iface_config->dhcp_start);
	g_free(iface_config->dhcp_end);
	g_list_free_full(iface_config->interfaces_list, remove_interface);
	g_hash_table_destroy(iface_config->setting_strings);
	__connman_ippool_unref(iface_config->ippool);
	g_free(iface_config);

    return;
}

static void connman_tethering_config_disable()
{
	struct connman_tether_config* config = NULL;
	static GHashTableIter iter;
	gpointer key, value;

	DBG("");
	g_hash_table_iter_init(&iter, tether_hash);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct connman_tether_config *config = value;
		if (NULL == config) {
			connman_error("%s: ERR : NULL pointer", __func__);
			continue;
		}
		connman_tethering_iface_config_disable(config);

	}
	return;
}

bool __connman_tethering_has_eth_config()
{
	struct connman_tether_config* config = NULL;
	config = g_hash_table_lookup(tether_hash, GINT_TO_POINTER(CONNMAN_SERVICE_TYPE_ETHERNET));
	if (NULL != config) {
		return true;
	}
	return false;
}

const char* __connman_tethering_get_tether_name(struct connman_tether_iface_config* iface_config)
{
	if (NULL == iface_config) {
		connman_error("%s: ERR : iface is NULL", __func__);
		return NULL;
	}
	return iface_config->tether_name;
}

static void remove_tether_config(gpointer user_data)
{
	struct connman_tether_config *config = user_data;

	DBG("");
	if (NULL == config) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return;
	}

	g_free(config->name);
	g_free(config->description);
	g_hash_table_destroy(config->tether_iface_hash);
	g_free(config);
}

static int set_string(struct connman_tether_iface_config *iface_config,
                    const char *key, const char *value)
{
	DBG("connman_tether_iface_config %p key %s value %s", iface_config, key, value);

	if (g_str_equal(key, "Interface")) {
		g_free(iface_config->interfaces);
		iface_config->interfaces = g_strdup(value);
	} else if (g_str_equal(key, "Netmask")) {
		/* Use hard code currently, only support netmask /24 */
	} else if (g_str_equal(key, "Host")) {
		g_free(iface_config->host);
		iface_config->host = g_strdup(value);
	} else if (g_str_equal(key, "Client")) {
		g_free(iface_config->client);
		iface_config->client= g_strdup(value);
	} else if (g_str_equal(key, "DHCP")) {
		g_free(iface_config->dhcp);
		iface_config->dhcp= g_strdup(value);
	} else {
		connman_warn("not handled key %s value %s", key, value);
	}

	g_hash_table_replace(iface_config->setting_strings,
				g_strdup(key), g_strdup(value));
	return 0;
}

static const char *get_string(struct connman_tether_iface_config* iface_config,
                            const char *key)
{
	DBG("provider %p key %s", iface_config, key);

	if (g_str_equal(key, "Interface"))
		return iface_config->interfaces;
	else if (g_str_equal(key, "Netmask"))
		return iface_config->netmask;
	else if (g_str_equal(key, "Host"))
		return iface_config->host;
	else if (g_str_equal(key, "Client"))
		return iface_config->client;
	else if (g_str_equal(key, "DHCP"))
		return iface_config->dhcp;

	return g_hash_table_lookup(iface_config->setting_strings, key);
}

static bool parse_dhcp_range(const char *dhcp_range_str, struct connman_tether_iface_config* iface_config)
{
	GSList *dhcp_range = NULL;
	char **elems;
	int i = 0;
	int count = 0;
	int err = 0;

	DBG("");
	if (!dhcp_range_str) {
		connman_error("ERR: invalid dhcp_range_str");
		return false;
	}

	elems = g_strsplit(dhcp_range_str, ",", 0);
	if (!elems) {
		connman_error("ERR: no network value");
		return false;
	}

	connman_info("%s: start parse", __func__);
	while (elems[i]) {
		char *network, *netmask, *gateway;
		int family;
		char **route;

		if (count >= 2) {
			connman_warn("WARN: more than 2 ip address : i=%d", i);
			break;
		}

		route = g_strsplit(elems[i], "/", 0);
		if (!route)
			goto next;

		network = route[0];
		if (!network || network[0] == '\0')
			goto next;

		connman_info("%s: dhcp network is %s, i=%d", __func__, network, i);
		family = connman_inet_check_ipaddress(network);
		if (family < 0) {
			DBG("Cannot get address family of %s (%d/%s)", network,
				family, gai_strerror(family));

			goto next;
		}

		switch (family) {
		case AF_INET:
			break;
		case AF_INET6:
			connman_error("ERR: not support ipv6 : i=%d", i);
			/* no break */
		default:
			DBG("Unsupported address family %d", family);
			goto next;
		}

		switch (count) {
		case 0:
			iface_config->dhcp_start = g_strdup(network);
			break;
		case 1:
			iface_config->dhcp_end = g_strdup(network);
			break;
		default:
			connman_warn("WARN: unexpect value : count=%d", count);
			goto next;
		}

		count++;

	next:
		g_strfreev(route);
		i++;
	}

	g_strfreev(elems);

	if (count != 2) {
		connman_error("ERR: unexpect error : count = %d", count);
		return false;
	}

	return true;
}

static void add_keys(struct connman_tether_iface_config *iface_config,
						GKeyFile *keyfile, const char *group)
{
	char **avail_keys;
	gsize nb_avail_keys, i;

	avail_keys = g_key_file_get_keys(keyfile, group, &nb_avail_keys, NULL);
	if (!avail_keys)
		return;

	for (i = 0 ; i < nb_avail_keys; i++) {
		char *value = g_key_file_get_value(keyfile, group,
						avail_keys[i], NULL);
		if (!value) {
			connman_warn("Cannot find value for %s",
						    avail_keys[i]);
			continue;
		}

		set_string(iface_config, avail_keys[i], value);
		g_free(value);
	}

	g_strfreev(avail_keys);
}

static bool is_private_address(const char* ip_addr)
{
	uint32_t address;
	unsigned int a, b;

	if (NULL == ip_addr)
		return false;

	struct in_addr inp;
	if (inet_aton(ip_addr, &inp) == 0)
		return false;

	address = ntohl(inp.s_addr);

	a = (address & 0xff000000) >> 24;
	b = (address & 0x00ff0000) >> 16;

	if (a == 10 || (a == 192 && b == 168) ||
			(a == 172 && (b >= 16 && b <= 31)))
		return true;

	return false;
}

static bool init_interfaces_list(struct connman_tether_iface_config *iface_config, char** interfaces)
{
	static GHashTableIter iter;
	static int len = 0;
	gpointer key, value;

	DBG("");
	if (NULL == iface_config) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return false;
	}
	int i = 0;
	while (interfaces[i]) {
		struct connman_tether_interface* iface = g_try_new0(struct connman_tether_interface, 1);
		if (NULL == iface) {
			connman_error("%s: ERR : no enough memory", __func__);
			return false;
		}
		iface->name = g_strdup(interfaces[i]);
		iface->is_found = false;
		iface_config->interfaces_list = g_list_append(iface_config->interfaces_list, iface);

		i++;
	}

	return true;
}

static int load_tether_iface(GKeyFile *keyfile, const char *group,
						struct connman_tether_config *config)
{
	struct connman_tether_iface_config *iface_config;
	const char *tether_name;
	const char *interfaces_str, *host, *client, *dhcp_range;
	int err;

	tether_name = group;

	if (strlen(tether_name) < 7 || strlen(tether_name) > 20) {
		connman_error("ERR: invalid tether_name=%s", tether_name ? tether_name : "(nil)");
		return -EINVAL;
	}
	connman_info("%s: %s", __func__, tether_name);

	iface_config = g_hash_table_lookup(config->tether_iface_hash, tether_name);
	if (iface_config) {
		connman_error("ERR: already exit : tether_name=%s", tether_name);
		return -EALREADY;
	}

	iface_config = g_try_new0(struct connman_tether_iface_config, 1);
	if (!iface_config)
		return -ENOMEM;

	iface_config->bridge_index = -1;
	iface_config->type = config->type;
	iface_config->tether_name = g_strdup(group);
	iface_config->interfaces_list = NULL;

	iface_config->setting_strings = g_hash_table_new_full(g_str_hash,
								g_str_equal, g_free, g_free);

	add_keys(iface_config, keyfile, group);

	/* special process for netmask */
	g_free(iface_config->netmask);
	iface_config->netmask = g_strdup(SUBNET_MASK_24_ADDRESS);

	interfaces_str = get_string(iface_config, "Interface");
	host = get_string(iface_config, "Host");
	client = get_string(iface_config, "Client");
	dhcp_range = get_string(iface_config, "DHCP");
	if (NULL != host && NULL != interfaces_str) {
		connman_info("%s: handle config item", __func__);
		if (AF_INET != connman_inet_check_ipaddress(host)
				|| AF_INET != connman_inet_check_ipaddress(iface_config->netmask)
				|| false == is_private_address(host)) {
			connman_error("%s: ERR : invalid ipaddress", __func__);
			err = -EINVAL;
			goto err;
		}
		char** interfaces;
		GError *error = NULL;
		gsize len;
		interfaces = __connman_config_get_string_list(keyfile, iface_config->tether_name,
                                            "Interface", &len, &error);
		if (error) {
			connman_error("%s: ERR : parse interfaces error", __func__);
			err = -EINVAL;
			g_strfreev(interfaces);
			goto err;
		}

		if (client && AF_INET == connman_inet_check_ipaddress(client)
			&& true == init_interfaces_list(iface_config, interfaces)) {
			connman_info("%s: client is valid", __func__);
			iface_config->dhcp_start = g_strdup(client);
			iface_config->dhcp_end = g_strdup(client);

			g_strfreev(interfaces);
		}
		else if (true == parse_dhcp_range(dhcp_range, iface_config) 
				&& true == init_interfaces_list(iface_config, interfaces)) {
			connman_info("%s: dhcp is valid", __func__);
			g_strfreev(interfaces);
		}
		else {
			connman_error("%s: ERR : dhcp range has some error in parse", __func__);
			g_strfreev(interfaces);
			goto err;
		}

		iface_config->ippool = __connman_ippool_reserve(iface_config->bridge_index,
								iface_config->dhcp_start,
								iface_config->dhcp_end,
								iface_config->host,
								iface_config->netmask,
								tethering_ip_pool_conflict,
								NULL);
		if (NULL == iface_config->ippool) {
			connman_error("%s: ERR : get reserved ip pool failed.", __func__);
			goto err;
		}
	} else {
		connman_error("%s: ERR: invalid values host %s interfaces %s", __func__,
					            host ? host : "(nil)", interfaces_str ? interfaces_str : "(nil)");
		err = -EINVAL;
		goto err;
	}

	DBG("tether=%s ifaces=%s host=%s netmask=%s client=%s dhcp_start=%s dhcp_end=%s",
		iface_config->tether_name,
		iface_config->interfaces,
		iface_config->host,
		iface_config->netmask,
		iface_config->client,
		iface_config->dhcp_start,
		iface_config->dhcp_end);

	g_hash_table_insert(config->tether_iface_hash,
				iface_config->tether_name, iface_config);

	if (err != 0) {
		connman_error("Cannot create tether iface from config file (%d/%s)",
					-err, strerror(-err));
		goto err;
	}

	connman_info("%s: Added tether iface configuration %s", __func__, iface_config->tether_name);
	return 0;

err:
	iface_config->bridge_index = -1;
	g_free(iface_config->tether_name);
	g_free(iface_config->interfaces);
	g_free(iface_config->host);
	g_free(iface_config->netmask);
	g_free(iface_config->client);
	g_free(iface_config->dhcp);
	g_free(iface_config->dhcp_start);
	g_free(iface_config->dhcp_end);
	g_list_free_full(iface_config->interfaces_list, remove_interface);
	g_hash_table_destroy(iface_config->setting_strings);
	__connman_ippool_unref(iface_config->ippool);
	g_free(iface_config);
	return err;
}

static GKeyFile *storage_load(const char *pathname)
{
	GKeyFile *keyfile = NULL;
	GError *error = NULL;

	DBG("Loading %s", pathname);

	keyfile = g_key_file_new();

	g_key_file_set_list_separator(keyfile, ',');
	if (!g_key_file_load_from_file(keyfile, pathname, 0, &error)) {
		DBG("Unable to load %s: %s", pathname, error->message);
		g_clear_error(&error);

		g_key_file_free(keyfile);
		keyfile = NULL;
	}

	return keyfile;
}

static int load_config(const char* config_file, struct connman_tether_config* config)
{
	gchar *pathname;
	GKeyFile *keyfile;
	gsize length;
	char **groups;
	char *str;
	bool found = false;
	int i;
	struct connman_tether_config* config_exist = NULL;

	DBG("config %p config_file %s", config, config_file);
	if (NULL == config_file) {
		connman_error("%s: ERR : NULL pointer", __func__);
		return -EINVAL;
	}

	pathname = g_strdup_printf("%s/%s.config", CONFIGDIR, config_file);
	connman_info("%s: path", pathname);
	keyfile = storage_load(pathname);
	if (!keyfile) {
		connman_error("ERR: load %s.config failed", config_file);
		return -EIO;
	}

	config->name = g_strdup(config_file);
	config->type = CONNMAN_SERVICE_TYPE_ETHERNET;
	config->description = g_strdup("");
	config->tether_iface_hash =
		g_hash_table_new_full(g_str_hash, g_str_equal, NULL, remove_iface_config);

	groups = g_key_file_get_groups(keyfile, &length);

	for (i = 0; groups[i]; i++) {
		if (g_str_has_prefix(groups[i], "tether")) {
			DBG("groups[%d] is %s", i, groups[i]);
			int ret = load_tether_iface(keyfile, groups[i], config);
			if (ret == 0 || ret == -EALREADY)
				found = true;
		}
	}

	if (!found)
		connman_warn("WARN: %s conf contains no useful information ", config_file);

	g_strfreev(groups);

	g_key_file_free(keyfile);

	return 0;
}


int __connman_tethering_init(void)
{
	DBG("");

	tethering_enabled = 0;
	tethering_config_enabled = false;

	connection = connman_dbus_get_connection();
	if (!connection)
		return -EFAULT;

	pn_hash = g_hash_table_new_full(g_str_hash, g_str_equal,
						NULL, remove_private_network);

	tether_hash = g_hash_table_new_full(g_direct_hash, g_direct_equal,
						NULL, remove_tether_config);
	struct connman_tether_config* conf = g_try_new0(struct connman_tether_config, 1);
	if (0 == load_config(TETHER_ETH_CONFIG_FILE, conf)) {
		enum connman_service_type type = conf->type;
		g_hash_table_replace(tether_hash, GINT_TO_POINTER(type), conf);
	} else {
		connman_warn("%s: WARN: not load config file", __func__);
		g_free(conf);
	}
	connman_info("%s: finish", __func__);
	return 0;
}

void __connman_tethering_cleanup(void)
{
	DBG("enabled %d", tethering_enabled);

	__sync_synchronize();
	if (tethering_config_enabled) {
		connman_info("%s: clean ethernet tethering with config", __func__);
		connman_tethering_config_disable();
	}
	if (tethering_enabled > 0) {
		connman_info("%s: clean default tethering", __func__);
		if (tethering_dhcp_server)
			dhcp_server_stop(tethering_dhcp_server);
		__connman_bridge_disable(BRIDGE_NAME);
		__connman_bridge_remove(BRIDGE_NAME);
		__connman_nat_disable(BRIDGE_NAME);
	}

	if (!connection)
		return;

	g_hash_table_destroy(tether_hash);
	g_hash_table_destroy(pn_hash);
	dbus_connection_unref(connection);
}
