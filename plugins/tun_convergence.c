/*

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the
disclaimer below) provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.

   * Neither the name of The Linux Foundation nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <glib.h>

#ifndef IFF_LOWER_UP
#define IFF_LOWER_UP	0x10000
#endif

#define CONNMAN_API_SUBJECT_TO_CHANGE
#include <connman/technology.h>
#include <connman/plugin.h>
#include <connman/device.h>
#include <connman/inet.h>
#include <connman/rtnl.h>
#include <connman/log.h>
#include <connman/setting.h>

struct tun_convergence_data {
	int index;
	char *name;
	char *path;
	unsigned flags;
	unsigned int watch;
	struct connman_network *network;
	struct connman_device *device;
};

static int tun_convergence_tech_probe(struct connman_technology *technology)
{
	DBG("-->tun_convergence_tech_probe");
	DBG("<--tun_convergence_tech_probe");
	return 0;
}

static void tun_convergence_tech_remove(struct connman_technology *technology)
{
	DBG("-->tun_convergence_tech_remove");
	DBG("<--tun_convergence_tech_remove");
}

static GList *tun_convergence_interface_list = NULL;

static void tun_convergence_tech_add_interface(struct connman_technology *technology,
			int index, const char *name, const char *ident)
{
	DBG("-->tun_convergence_tech_add_interface");
	DBG("tun_convergence_tech_add_interface === index %d name %s ident %s", index, name, ident);

	if (g_list_find(tun_convergence_interface_list, GINT_TO_POINTER((int)index)))
		return;

	tun_convergence_interface_list = g_list_prepend(tun_convergence_interface_list,
					(GINT_TO_POINTER((int) index)));
	DBG("<--tun_convergence_tech_add_interface");
}

static void tun_convergence_tech_remove_interface(struct connman_technology *technology,
								int index)
{
	DBG("-->tun_convergence_tech_remove_interface");
	DBG("index %d", index);

	tun_convergence_interface_list = g_list_remove(tun_convergence_interface_list,
					GINT_TO_POINTER((int) index));
	DBG("<--tun_convergence_tech_remove_interface");
}

static struct connman_technology_driver tun_convergence_tech_driver = {
	.name			= "tun_convergence",
	.type			= CONNMAN_SERVICE_TYPE_TUN_CONVERGENCE,
	.probe			= tun_convergence_tech_probe,
	.remove			= tun_convergence_tech_remove,
	.add_interface		= tun_convergence_tech_add_interface,
	.remove_interface	= tun_convergence_tech_remove_interface,
};

static int tun_convergence_network_probe(struct connman_network *network)
{

	DBG("-->tun_convergence_network_probe");
	DBG("network %p", network);
	DBG("<--tun_convergence_network_probe");
	return 0;
}

static void tun_convergence_network_remove(struct connman_network *network)
{
	DBG("-->tun_convergence_network_remove");
	DBG("network %p", network);
	DBG("<--tun_convergence_network_remove");
}

static int tun_convergence_network_connect(struct connman_network *network)
{
	DBG("-->tun_convergence_network_connect");
	connman_network_set_connected(network, true);
	DBG("<--tun_convergence_network_connect");
	return 0;
}

static int tun_convergence_network_disconnect(struct connman_network *network)
{
	DBG("-->tun_convergence_network_disconnect");
	DBG("network %p", network);

	connman_network_set_connected(network, false);
	DBG("<--tun_convergence_network_disconnect");
	return 0;
}

static struct connman_network_driver tun_convergence_network_driver = {
	.name		= "tun_convergence",
	.type		= CONNMAN_NETWORK_TYPE_TUN_CONVERGENCE,
	.probe		= tun_convergence_network_probe,
	.remove		= tun_convergence_network_remove,
	.connect	= tun_convergence_network_connect,
	.disconnect	= tun_convergence_network_disconnect,
};


static void add_network(struct connman_device *device,
			struct tun_convergence_data *tun_convergence)
{
	struct connman_network *network;
	int index;
	char *ifname;

	DBG("-->tun_convergence add_network device:%p data:%p", device, tun_convergence);

	network = connman_network_create("tun_convergence",
					CONNMAN_NETWORK_TYPE_TUN_CONVERGENCE);
	if (!network)
		return;

	index = connman_device_get_index(device);
	connman_network_set_index(network, index);
	ifname = connman_inet_ifname(index);
	if (!ifname)
		return;
	connman_network_set_name(network, "tun_convergence");
	connman_network_set_data(network, tun_convergence);
	if (connman_device_add_network(device, network) < 0) {
        DBG("tun_convergence connman_device_add_network fails!!!");
		connman_network_unref(network);
		return;
	}
	connman_network_set_group(network, "convergence");

	tun_convergence->network = network;
	g_free(ifname);

	DBG("<--tun_convergence add_network");
}

static void set_connected(struct tun_convergence_data *tun_convergence)
{
	int fd;
	struct ifreq ifr;
	struct connman_service *service;
	struct connman_ipaddress *ipv4_address;

	DBG("-->set_connected");
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, "convergence0", IFNAMSIZ-1);
	ioctl(fd, SIOCGIFADDR, &ifr);
	ipv4_address = connman_ipaddress_alloc(CONNMAN_IPCONFIG_TYPE_IPV4);
	service = connman_service_lookup_from_network(tun_convergence->network);
	if (!service)
		return;
	connman_service_create_ip4config(service, tun_convergence->index);

	connman_network_set_ipv4_method(tun_convergence->network, CONNMAN_IPCONFIG_METHOD_MANUAL);
	connman_ipaddress_set_ipv4(ipv4_address, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr), "255.255.255.255", "0.0.0.0");
	connman_network_set_ipaddress(tun_convergence->network, ipv4_address);
	connman_ipaddress_free(ipv4_address);
	// We don't really need a DNS server for our convergence plugin, put a dummy one
	connman_network_set_nameservers(tun_convergence->network, "8.8.8.8");
	connman_network_set_connected(tun_convergence->network, true);
	DBG("<--set_connected");
}

static void set_disconnected(struct tun_convergence_data *tun_convergence)
{
	DBG("-->set_disconnected");
	connman_network_set_connected(tun_convergence->network, false);
	DBG("<--set_disconnected");
}

static void remove_network(struct connman_device *device,
				struct tun_convergence_data *tun_convergence)
{
	DBG("-->remove_network");
	if (!tun_convergence->network)
		return;

	connman_device_remove_network(device, tun_convergence->network);
	connman_network_unref(tun_convergence->network);

	tun_convergence->network = NULL;
	DBG("<--remove_network");
}

static void tun_convergence_newlink(unsigned flags, unsigned change, void *user_data)
{
	struct connman_device *device = user_data;
	struct tun_convergence_data *tun_convergence = connman_device_get_data(device);

	DBG("-->tun_convergence_newlink index %d flags %d change %d", tun_convergence->index, flags, change);

	if ((tun_convergence->flags & IFF_UP) != (flags & IFF_UP)) {
		if (flags & IFF_UP) {
			DBG("power on");
			connman_device_set_powered(device, true);
		} else {
			DBG("power off");
			connman_device_set_powered(device, false);
		}
	}

	if ((tun_convergence->flags & IFF_LOWER_UP) != (flags & IFF_LOWER_UP)) {
		if (flags & IFF_LOWER_UP) {
			DBG("carrier on");
			add_network(device, tun_convergence);
            set_connected(tun_convergence);
		} else {
			DBG("carrier off");
            set_disconnected(tun_convergence);
			remove_network(device, tun_convergence);
		}
	}
	tun_convergence->flags = flags;
	DBG("<--tun_convergence_newlink");
}

static int tun_convergence_dev_probe(struct connman_device *device)
{
	struct tun_convergence_data *tun_convergence;

	DBG("-->tun_convergence_dev_probe==== device %p data %p", device, tun_convergence);

	tun_convergence = g_try_new0(struct tun_convergence_data, 1);
	if (!tun_convergence)
		return -ENOMEM;

	connman_device_set_data(device, tun_convergence);

	tun_convergence->device = device;
	tun_convergence->index = connman_device_get_index(device);
	connman_device_set_ident(device, "mpscheduler");
	tun_convergence->flags = 0;
	tun_convergence->watch = connman_rtnl_add_newlink_watch(tun_convergence->index,
						tun_convergence_newlink, device);

	DBG("-->tun_convergence_dev_probe");

	return 0;
}


static void tun_convergence_dev_remove(struct connman_device *device)
{
	struct tun_convergence_data *tun_convergence = connman_device_get_data(device);

	DBG("-->tun_convergence_dev_remove");

	DBG("device %p", device);

	connman_device_set_data(device, NULL);

	remove_network(device, tun_convergence);

	g_free(tun_convergence);

	DBG("<--tun_convergence_dev_remove");
}

static int tun_convergence_dev_enable(struct connman_device *device)
{
	struct tun_convergence_data *tun_convergence = connman_device_get_data(device);

	DBG("-->tun_convergence_dev_enable");

	DBG("device %p", device);

	DBG("<--tun_convergence_dev_enable");

	return connman_device_set_powered(device, TRUE);
}

static int tun_convergence_dev_disable(struct connman_device *device)
{
	struct tun_convergence_data *tun_convergence = connman_device_get_data(device);

	DBG("-->tun_convergence_dev_disable");

	DBG("device %p", device);

	DBG("<--tun_convergence_dev_disable");

	return connman_device_set_powered(device, FALSE);
}

static struct connman_device_driver tun_convergence_dev_driver = {
	.name		= "tun_convergence",
	.type		= CONNMAN_DEVICE_TYPE_TUN_CONVERGENCE,
	.probe		= tun_convergence_dev_probe,
	.remove		= tun_convergence_dev_remove,
	.enable		= tun_convergence_dev_enable,
	.disable	= tun_convergence_dev_disable,
};

static int tun_convergence_init(void)
{
	int err;

	DBG("-->tun_convergence_init");

	if (err < 0){
		DBG("create_tun_convergence_device, error %d", err);
		return err;
	}

	err = connman_network_driver_register(&tun_convergence_network_driver);
	if (err < 0){
		DBG("connman_network_driver_register fails, error %d", err);
		connman_network_driver_unregister(&tun_convergence_network_driver);
		return err;
	}

	err = connman_device_driver_register(&tun_convergence_dev_driver);
	if (err < 0) {
		DBG("connman_device_driver_register fails, error %d", err);
		connman_device_driver_unregister(&tun_convergence_dev_driver);
		return err;
	}

	err = connman_technology_driver_register(&tun_convergence_tech_driver);
	if (err < 0) {
		DBG("connman_technology_driver_register fails, error %d", err);
		connman_technology_driver_unregister(&tun_convergence_tech_driver);
		return err;
	}

	DBG("<--tun_convergence_init");

	return 0;
}

static void tun_convergence_exit(void)
{

	DBG("<--tun_convergence_exit");

	connman_technology_driver_unregister(&tun_convergence_tech_driver);

	connman_network_driver_unregister(&tun_convergence_network_driver);

	connman_device_driver_unregister(&tun_convergence_dev_driver);

	DBG("-->tun_convergence_exit");
}

CONNMAN_PLUGIN_DEFINE(tun_convergence, "tun_convergence device plugin", VERSION,
		CONNMAN_PLUGIN_PRIORITY_HIGH, tun_convergence_init, tun_convergence_exit)
