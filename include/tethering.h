/*
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:

 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.

 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.

 * * Neither the name of The Linux Foundation nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.

 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __CONNMAN_TETHERING_H
#define __CONNMAN_TETHERING_H

#include <connman/service.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * SECTION:tethering
 * @title: tethering premitives
 * @short_description: Functions for handling tethering details
 */

struct connman_tether_iface_config;

/* For Ethernet Multi-tethering Feature */
void __connman_tethering_eth_set_enabled(struct connman_tether_iface_config* iface_config);
void __connman_tethering_eth_set_disabled(struct connman_tether_iface_config* iface_config);
bool __connman_tethering_has_eth_config(void);
struct connman_tether_iface_config* __connman_tethering_lookup_eth_iface_config(const char* ifname);
void __connman_technology_update_config_tethering(enum connman_service_type type, bool is_tethering);
const char* __connman_tethering_get_tether_name(struct connman_tether_iface_config* iface_config);
void __connman_tethering_set_interface_found(enum connman_service_type type, const char* ifname);
bool __connman_tethering_all_eth_iface_is_up(void);

#ifdef __cplusplus
}
#endif

#endif /* __CONNMAN_TETHERING_H */
